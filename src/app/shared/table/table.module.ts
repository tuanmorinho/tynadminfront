import {NgModule} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {
    NbActionsModule, NbCheckboxModule,
    NbFormFieldModule,
    NbIconModule,
    NbInputModule,
    NbSelectModule,
    NbTreeGridModule
} from "@nebular/theme";
import {TableComponent} from './table.component';
import {Ng2SmartTableModule} from "ng2-smart-table";
import {CellImageComponent} from './components/cell-image/cell-image.component';
import {CellCustomComponent} from "@shared/table/components/cell-custom/cell-custom.component";
import {CellStatusChangeComponent} from './components/cell-status-change/cell-status-change.component';
import {ReactiveFormsModule} from "@angular/forms";
import {CellCheckboxComponent} from "@shared/table/components/cell-checkbox/cell-checkbox.component";
import { CellPriorityComponent } from './components/cell-priority/cell-priority.component';

@NgModule({
    declarations: [
        TableComponent,
        CellCustomComponent,
        CellImageComponent,
        CellStatusChangeComponent,
        CellCheckboxComponent,
        CellPriorityComponent
    ],
    imports: [
        CommonModule,
        NbTreeGridModule,
        Ng2SmartTableModule,
        NbActionsModule,
        NbIconModule,
        NbSelectModule,
        NbFormFieldModule,
        ReactiveFormsModule,
        NbInputModule,
        NgOptimizedImage,
        NbCheckboxModule
    ],
    exports: [
        TableComponent,
        CellCustomComponent,
        CellImageComponent,
        CellStatusChangeComponent,
        CellCheckboxComponent,
        CellPriorityComponent
    ]
})
export class TableModule {
}
