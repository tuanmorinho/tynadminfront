import {Component, EventEmitter, inject, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {LocalDataSource} from "ng2-smart-table";
import {Observable, takeUntil} from "rxjs";
import {IAuthUser, PageSizes} from "@core/model/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ITableState} from "@core/model/core/table-state.model";
import {BaseComponent} from "@core/module/base/base.component";
import {AuthService} from "@core/services/core";

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss'],
})
export class TableComponent<T> extends BaseComponent implements OnInit, OnChanges {

    @Input() data!: Observable<T[]>;
    @Input() tableSettings: any = {actions: {edit: true, delete: true, columnTitle: null, position: 'left'}};
    @Input() columns: any;
    @Input() tableState!: Observable<ITableState>;

    @Input('permissionActions') set permissionActions(val: string[]) {
        this.permissions = val;
        this.checkView();
    }

    @Output() paginator: EventEmitter<any> = new EventEmitter<any>();
    @Output() edit: EventEmitter<any> = new EventEmitter<any>();
    @Output() delete: EventEmitter<any> = new EventEmitter<any>();

    private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
    private authService: AuthService = inject<AuthService>(AuthService);

    settings: any = {
        mode: 'external',
        hideSubHeader: true,
        actions: {
            add: false,
            columnTitle: null,
            position: 'left',
        },
        edit: {
            editButtonContent: '<img src="../../../assets/svg/edit-2.svg" width="25" height="25" alt="_"/>'
        },
        delete: {
            deleteButtonContent: '<img src="../../../assets/svg/trash-2.svg" width="25" height="25" alt="_"/>'
        },
        pager: {
            display: false
        },
        attr: {
            class: 'table'
        },
    };
    source: LocalDataSource = new LocalDataSource();

    private permissions: string[] = [];
    private isHidden: boolean = true;

    private total: number = 0;
    offset: number = 0;
    paginatorForm!: FormGroup;
    protected readonly PageSizes: number[] = PageSizes;

    ngOnInit(): void {
        this.paginatorForm = this.fb.group({
            take: [PageSizes[0], Validators.required],
            page: [1, Validators.required]
        });

        this.setTableSettings();
        this.setColumn();
        this.setData();
        this.setPagination();

        this.watchPaginationChange();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.setTableSettings();
    }

    get v() {
        return this.paginatorForm.value;
    }

    private setTableSettings(): void {
        this.settings = {
            ...this.settings,
            ...this.tableSettings
        }
        this.settings = {...this.settings}
    }

    private setColumn(): void {
        this.settings = {
            ...this.settings,
            columns: this.columns
        }
        this.settings = {...this.settings}
    }

    private setData(): void {
        this.data.pipe(takeUntil(this._destroy$)).subscribe({
            next: (data: T[]): void => {
                this.source.load(data);
            }
        });
    }

    private setPagination(): void {
        this.tableState.pipe(takeUntil(this._destroy$)).subscribe({
            next: (tableState: ITableState): void => {
                this.total = tableState.paginator?.total;
                this.offset = Math.round(tableState.paginator?.total / this.v.take) !== 0 ? Math.round(tableState.paginator?.total / this.v.take) : 1;
            }
        })
    }

    private watchPaginationChange(): void {
        this.paginatorForm.valueChanges.pipe(takeUntil(this._destroy$)).subscribe({
            next: ({take, page}): void => {
                this.offset = Math.round(this.total / take) !== 0 ? Math.round(this.total / take) : 1;
                console.log(this.offset)
            }
        })
    }

    changePage(): void {
        this.toStart();
    }

    toStart(): void {
        this.paginatorForm.controls['page'].setValue(1);
        this.paginator.emit(this.v);
    }

    toPrev(): void {
        if (this.v.page > 1)
            this.paginatorForm.controls['page'].setValue(this.v.page -= 1);
        this.paginator.emit(this.v);
    }

    toNext(): void {
        if (this.v.page < this.offset)
            this.paginatorForm.controls['page'].setValue(this.v.page += 1);
        this.paginator.emit(this.v);
    }

    toEnd(): void {
        this.paginatorForm.controls['page'].setValue(this.offset);
        this.paginator.emit(this.v);
    }

    editFn(evt: any): void {
        if (evt) this.edit.emit(evt);
    }

    deleteFn(evt: any): void {
        if (evt) this.delete.emit(evt);
    }

    private checkView(): void {
        if (this.checkPermission()) {
            if (this.isHidden) {
                this.settings.actions = Object.assign(this.settings.actions, this.canEditAndDelete)
                this.isHidden = true;
            }
        } else {
            this.isHidden = false;
            this.settings.actions = Object.assign(this.settings.actions, this.canNotEditAndDelete)
        }
    }

    private checkPermission(): boolean {
        const currentUser: IAuthUser | any = this.authService.getUserByToken();

        if (currentUser && currentUser.roles.filter((role: string): boolean => role === 'ADMIN').length > 0) {
            return true;
        }

        let hasPermission: boolean = false;

        if (currentUser && currentUser.roles) {
            const currentPermissions = currentUser.roles || [];
            const permissionFound = currentPermissions.filter((role: string): boolean => this.permissions.indexOf(role) !== -1);
            if (permissionFound.length !== 0) {
                hasPermission = true
            } else hasPermission = false
        }
        return hasPermission;
    }

    private get canEditAndDelete(): { edit: boolean, delete: boolean } {
        return {
            edit: true,
            delete: true
        }
    }

    private get canNotEditAndDelete(): { edit: boolean, delete: boolean } {
        return {
            edit: false,
            delete: false
        }
    }
}
