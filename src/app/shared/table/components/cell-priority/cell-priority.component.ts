import {Component, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'app-cell-priority',
    templateUrl: './cell-priority.component.html',
    styleUrls: ['./cell-priority.component.scss']
})
export class CellPriorityComponent {
    @Output() change: EventEmitter<any> = new EventEmitter<any>();

    sortPriority(): void {
        this.change.emit();
    }
}
