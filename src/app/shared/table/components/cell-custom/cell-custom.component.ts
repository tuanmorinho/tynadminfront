import {Component, Input} from '@angular/core';
import {ICellCustom} from "@core/model/core";

@Component({
  selector: 'app-table-cell-custom',
  templateUrl: './cell-custom.component.html',
  styleUrls: ['./cell-custom.component.scss']
})
export class CellCustomComponent {
  @Input() value!: ICellCustom;
}
