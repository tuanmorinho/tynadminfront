import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ICellStatusChange} from "@core/model/core";

@Component({
  selector: 'app-cell-status-change',
  templateUrl: './cell-status-change.component.html',
  styleUrls: ['./cell-status-change.component.scss']
})
export class CellStatusChangeComponent {
  @Input() value!: ICellStatusChange;
  @Input() rowData: any;
  @Output() change: EventEmitter<any> = new EventEmitter();

  selectedChange(value: any): void {
    this.change.emit({ value, row: this.rowData });
  }
}
