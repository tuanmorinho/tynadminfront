import {Component, Input} from '@angular/core';
import {ICellImage} from "@core/model/core";

@Component({
  selector: 'app-cell-image',
  templateUrl: './cell-image.component.html',
  styleUrls: ['./cell-image.component.scss']
})
export class CellImageComponent {
  @Input() value!: ICellImage;
}
