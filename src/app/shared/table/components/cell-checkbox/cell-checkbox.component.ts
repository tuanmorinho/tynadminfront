import {Component, Input} from '@angular/core';
import {ICellCheckbox} from "@core/model/core";

@Component({
    selector: 'app-cell-checkbox',
    templateUrl: './cell-checkbox.component.html',
    styleUrls: ['./cell-checkbox.component.scss']
})
export class CellCheckboxComponent {
    @Input() value!: ICellCheckbox;
}
