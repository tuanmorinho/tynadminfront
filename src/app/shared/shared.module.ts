import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableModule} from "@shared/table/table.module";
import {FilterModule} from "@shared/filter/filter.module";
import {ImageModule} from "@shared/image/image.module";
import {FileModule} from "@shared/file/file.module";
import {SortPriorityDialogComponent} from './components/sort-priority-dialog/sort-priority-dialog.component';
import {NbButtonModule, NbCardModule, NbIconModule} from "@nebular/theme";
import {DirectiveModule} from "@core/directive/directive.module";


@NgModule({
    declarations: [
        SortPriorityDialogComponent
    ],
    imports: [
        CommonModule,
        NbCardModule,
        NbButtonModule,
        DirectiveModule,
        NbIconModule
    ],
    exports: [
        TableModule,
        FilterModule,
        ImageModule,
        FileModule,

        SortPriorityDialogComponent
    ],
})
export class SharedModule {
}
