import {NgModule} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {ImageFrameComponent} from './image-frame/image-frame.component';
import {ImgDisplayComponent} from './img-display/img-display.component';
import {NbIconModule} from "@nebular/theme";

@NgModule({
  declarations: [
    ImageFrameComponent,
    ImgDisplayComponent
  ],
    imports: [
        CommonModule,
        NgOptimizedImage,
        NbIconModule
    ],
  exports: [
    ImageFrameComponent,
    ImgDisplayComponent
  ]
})
export class ImageModule {
}
