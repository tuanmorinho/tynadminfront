import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {environment} from "../../../../enviroments";

@Component({
  selector: 'app-img-display',
  templateUrl: './img-display.component.html',
  styleUrls: ['./img-display.component.scss']
})
export class ImgDisplayComponent implements OnInit, OnChanges {
  @Input() url: string | undefined;
  @Input() width: number = 150;
  @Input() height: number = 150;

  baseUrl: string = environment.apiUrl.base;
  src: string = '';

  ngOnInit(): void {
    if (this.url) this.src = this.baseUrl + this.url;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes?.['url'].previousValue !== changes?.['url'].currentValue) {
      if (changes?.['url'].currentValue) this.src = this.baseUrl + changes?.['url'].currentValue;
    }
  }
}
