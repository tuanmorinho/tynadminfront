import {Component, EventEmitter, inject, Input, OnInit, Output} from '@angular/core';
import {NbWindowRef, NbWindowService} from "@nebular/theme";
import {FileUploadComponent} from "@shared/file/file-upload/file-upload.component";

@Component({
  selector: 'app-image-frame',
  templateUrl: './image-frame.component.html',
  styleUrls: ['./image-frame.component.scss']
})
export class ImageFrameComponent implements OnInit {

  @Output() changeImage: EventEmitter<string> = new EventEmitter<string>();
  @Output() clearImage: EventEmitter<string> = new EventEmitter<string>();
  @Input() src!: string;
  @Input() multiple: boolean = false;
  @Input() disabled: boolean = false;

  private windowService: NbWindowService = inject<NbWindowService>(NbWindowService);

  ngOnInit(): void {
  }

  change(): void {
    if (this.disabled) {
      return;
    }

    const windowRef: NbWindowRef = this.windowService.open(FileUploadComponent, {
      title: `Hệ thống file`,
      closeOnBackdropClick: false
    });
    windowRef.componentInstance.isChooseFile = true;

    windowRef.onClose.subscribe({
      next: (data): void => {
        if (data && data.length > 0) {
          this.changeImage.emit(data[0].url);
        }
      }
    });
  }

  close(): void {
    this.clearImage.emit();
  }

}
