import {AfterViewInit, Component, ElementRef, inject, Input} from '@angular/core';
import {NbDialogRef} from "@nebular/theme";
import {IBasePriorityModel} from "@core/model/core";
import {fromEvent, takeUntil, throttleTime} from "rxjs";
import {BaseComponent} from "@core/module/base/base.component";

@Component({
    selector: 'app-sort-priority-dialog',
    templateUrl: './sort-priority-dialog.component.html',
    styleUrls: ['./sort-priority-dialog.component.scss']
})
export class SortPriorityDialogComponent extends BaseComponent implements AfterViewInit {
    @Input() priorityList: IBasePriorityModel[] = [];

    private elRef: ElementRef = inject<ElementRef>(ElementRef);
    private dialogRef: NbDialogRef<SortPriorityDialogComponent> = inject<NbDialogRef<SortPriorityDialogComponent>>(NbDialogRef);

    ngAfterViewInit(): void {
        this.attachStyleEvent();
    }

    upWard(item: IBasePriorityModel): void {
        const currentIndex: number = this.priorityList.findIndex((v: IBasePriorityModel) => v.id === item.id);
        if (currentIndex > 0) {
            let tempItem: IBasePriorityModel = this.priorityList[currentIndex];
            let tempPriorityCurrent: number | undefined = this.priorityList[currentIndex]?.priority;
            let tempPriorityPrev: number | undefined = this.priorityList[currentIndex - 1]?.priority;
            this.priorityList[currentIndex] = this.priorityList[currentIndex - 1];
            this.priorityList[currentIndex].priority = tempPriorityCurrent;
            this.priorityList[currentIndex - 1] = tempItem;
            this.priorityList[currentIndex - 1].priority = tempPriorityPrev;
        }
    }

    downWard(item: IBasePriorityModel): void {
        const currentIndex: number = this.priorityList.findIndex((v: IBasePriorityModel) => v.id === item.id);
        if (currentIndex > -1 && currentIndex < this.priorityList.length - 1) {
            let tempItem: IBasePriorityModel = this.priorityList[currentIndex];
            let tempPriorityCurrent: number | undefined = this.priorityList[currentIndex]?.priority;
            let tempPriorityNext: number | undefined = this.priorityList[currentIndex + 1]?.priority;
            this.priorityList[currentIndex] = this.priorityList[currentIndex + 1];
            this.priorityList[currentIndex].priority = tempPriorityCurrent
            this.priorityList[currentIndex + 1] = tempItem;
            this.priorityList[currentIndex + 1].priority = tempPriorityNext;
        }
    }

    onSubmit(): void {
        this.dialogRef.close(this.priorityList);
    }

    back(): void {
        this.dialogRef.close();
    }

    private attachStyleEvent(): void {
        const root = this.elRef.nativeElement;
        const items: HTMLElement[] = Array.from(root.querySelectorAll(".sort-item")) as HTMLElement[];
        const iconUps: HTMLElement[] = Array.from(root.querySelectorAll(".ic_up_ward")) as HTMLElement[];
        const iconDowns: HTMLElement[] = Array.from(root.querySelectorAll(".ic_down_ward")) as HTMLElement[];
        if (iconUps && iconUps.length) {
            iconUps.forEach((icon: HTMLElement, index: number): void => {
                fromEvent(icon, 'click').pipe(
                    throttleTime(1000),
                    takeUntil(this._destroy$)
                ).subscribe({
                    next: (): void => {
                        items.forEach((item: HTMLElement): void => {
                            item.classList.remove("active-upward");
                            item.classList.remove("active-downward")
                        });
                        const parent: Element | null | undefined = icon.parentElement?.parentElement;
                        if (parent) parent.classList.add("active-upward");
                    }
                })
            })
        }

        if (iconDowns && iconDowns.length) {
            iconDowns.forEach((icon: HTMLElement, index: number): void => {
                fromEvent(icon, 'click').pipe(
                    throttleTime(1000),
                    takeUntil(this._destroy$)
                ).subscribe({
                    next: (): void => {
                        items.forEach((item: HTMLElement): void => {
                            item.classList.remove("active-upward");
                            item.classList.remove("active-downward")
                        });
                        const parent: Element | null | undefined = icon.parentElement?.parentElement;
                        if (parent) parent.classList.add("active-downward");
                    }
                })
            })
        }
    }
}
