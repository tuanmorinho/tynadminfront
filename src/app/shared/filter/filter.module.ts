import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NbButtonModule, NbCardModule, NbFormFieldModule, NbInputModule, NbSelectModule} from "@nebular/theme";
import {DirectiveModule} from "@core/directive/directive.module";



@NgModule({
  declarations: [
    FilterComponent
  ],
  exports: [
    FilterComponent
  ],
    imports: [
        CommonModule,
        FormsModule,
        NbButtonModule,
        NbCardModule,
        NbFormFieldModule,
        NbInputModule,
        NbSelectModule,
        ReactiveFormsModule,
        DirectiveModule
    ]
})
export class FilterModule { }
