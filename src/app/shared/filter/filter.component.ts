import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {IFilterModel} from "@core/model/core";

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent {

  @Input() formGroup!: FormGroup;
  @Input() items!: IFilterModel[];
  @Input() type: 'row' | 'column' = 'row';
  @Output() search: EventEmitter<any> = new EventEmitter<any>();
  @Output() reset: EventEmitter<any> = new EventEmitter<any>();

  submit(): void {
    this.search.emit(this.filterData);
  }

  resetFilter(): void {
    this.reset.emit();
  }

  private get filterData(): any {
    const v = this.formGroup.value;
    Object.keys(v).forEach((key: string): void => {
      if (v[key] === null || v[key] === '') {
        delete v[key];
      }
    });
    return v;
  }

}
