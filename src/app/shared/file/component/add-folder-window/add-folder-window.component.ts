import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {FormBuilder, Validators} from "@angular/forms";
import {NbDialogRef, NbDialogService} from "@nebular/theme";

@Component({
  selector: 'app-add-folder-window',
  templateUrl: './add-folder-window.component.html',
  styleUrls: ['./add-folder-window.component.scss']
})
export class AddFolderWindowComponent extends BaseComponent implements OnInit {

  private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
  private nbDialogRef: NbDialogRef<AddFolderWindowComponent> = inject<NbDialogRef<AddFolderWindowComponent>>(NbDialogRef);

  ngOnInit(): void {
    this.form = this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.maxLength(256)])]
    })
  }

  onSubmit(): void {
    this.form.markAllAsTouched();
    if (this.form.invalid) return;
    this.nbDialogRef?.close(this.form.value.name);
  }

}
