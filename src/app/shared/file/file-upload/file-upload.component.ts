import {Component, inject, Input, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {FileFolderService, FileUploadService} from "@core/services/core";
import {IFileFolder, IFileFolderListResponse, IFileForChoose, IFileStorage} from "@core/model/core";
import {forkJoin, of, switchMap, takeUntil} from "rxjs";
import {NbDialogRef, NbDialogService, NbToastrService, NbWindowRef} from "@nebular/theme";
import {AddFolderWindowComponent} from "@shared/file/component/add-folder-window/add-folder-window.component";

@Component({
    selector: 'app-file-upload',
    templateUrl: './file-upload.component.html',
    styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent extends BaseComponent implements OnInit {

    @Input() isChooseFile: boolean = false;
    @Input() multiple: boolean = false;

    fileFolder!: IFileFolderListResponse;
    folderList: IFileFolder[] = [];
    fileList: IFileForChoose[] = [];
    breadcrumbs: IFileFolder[] = [];

    readonly allowExtStr = '.png,.jpeg,.jpg,.svg';

    private fileFolderService: FileFolderService = inject<FileFolderService>(FileFolderService);
    private nbWindowRef: NbWindowRef = inject<NbWindowRef>(NbWindowRef);
    private nbDialogService: NbDialogService = inject<NbDialogService>(NbDialogService);
    private toastrService: NbToastrService = inject<NbToastrService>(NbToastrService);
    private fileUploadService: FileUploadService = inject<FileUploadService>(FileUploadService);

    ngOnInit(): void {
        this.findFolderRoot();
    }

    toBreadcrumb(folder?: IFileFolder | undefined): void {
        this.toFolder(folder);
    }

    toFolder(folder: IFileFolder | undefined): void {
        if (folder && folder.id) {
            this.fileFolderService.viewFolder({id: folder.id}).pipe(
                takeUntil(this._destroy$)
            ).subscribe({
                next: (data: IFileFolderListResponse): void => {
                    this.isLoading = false;
                    if (data) this.fileFolder = data;
                    if (data && data?.folderList) this.folderList = data?.folderList;
                    if (data && data?.fileList) {
                        this.fileList = data?.fileList.map(v => {
                            return {
                                ...v,
                                checked: false,
                                disable: false
                            }
                        });
                    }
                    this.updateBreadcrumb(folder);
                },
                error: (): void => {
                    this.isLoading = false;
                }
            });
        } else {
            this.findFolderRoot();
        }
    }

    onFileUploadChange(e: any): void {
        console.log(e)
        let fileList: File[] = [];
        for (let i: number = 0; i < e.target.files.length; i++) {
            const file: File = e.target.files[i];
            if (!file) return;
            const allowExt: string[] = this.allowExtStr.split(',').map(str => str.trim().replace('.', ''));
            if (!allowExt.some((ext: string) => file.type.toLowerCase().includes(ext))) {
                this.toastrService.warning('Ảnh không đúng định dạng quy định', 'Cảnh báo');
                return;
            }
            fileList.push(file);
        }

        forkJoin(fileList.map((file: File) => {
            const formData: FormData = new FormData();
            const folderParent: IFileFolder = this.breadcrumbs.at(-1) as IFileFolder;
            const fileFolderParentId: string = folderParent?.id ? folderParent?.id.toString() : '';
            formData.append('folderId', fileFolderParentId);
            formData.append('file', file);
            return this.fileUploadService.uploadFile(formData);
        })).subscribe({
            next: (data: IFileStorage[]): void => {
                if (data && data.length > 0) {
                    const newData = data.map(v => {
                        return {
                            ...v,
                            checked: false,
                            disable: false
                        }
                    });
                    this.fileList = newData.concat([...this.fileList]);
                }
                this.toastrService.success('Upload file thành công', 'Thành công');
            },
            error: (error): void => {
                this.toastrService.danger(error?.error?.message, 'Lỗi');
            }
        });
    }

    deleteFile(event: any, file: IFileStorage): void {
        event.stopPropagation();
        if (file && file.id) {
            this.fileUploadService.doDeleteFile(file).subscribe({
                next: (): void => {
                    const fileIndex: number = this.fileList.findIndex(v => v.name === file.name);
                    this.fileList.splice(fileIndex, 1);
                    this.toastrService.success('Xóa file thành công', 'Thành công');
                },
                error: (error): void => {
                    this.toastrService.danger(error.error.message, 'Lỗi');
                }
            });
        }
    }

    createFolder(): void {
        const dialogRef: NbDialogRef<AddFolderWindowComponent> = this.nbDialogService.open(AddFolderWindowComponent);
        dialogRef.onClose.subscribe(folderName => {
            if (folderName) {
                const folderParent: IFileFolder = this.breadcrumbs.at(-1) as IFileFolder;
                const fileFolderParentId: number | null = folderParent?.id ?? null;
                this.fileFolderService.doCreateFolder({name: folderName, fileFolderParentId}).pipe(
                    switchMap(() => {
                        this.toFolder(folderParent);
                        return of(null);
                    })
                ).subscribe();
            }
        })
    }

    deleteFolder(event: any, folder: IFileFolder): void {
        event.stopPropagation();
        if (folder && folder.id) {
            this.fileFolderService.doDeleteFolder(folder).subscribe({
                next: (): void => {
                    const folderIndex: number = this.folderList.findIndex(v => v.id === folder.id);
                    this.folderList.splice(folderIndex, 1);
                    this.toastrService.success('Xóa folder thành công', 'Thành công');
                },
                error: (error): void => {
                    this.toastrService.danger(error.error.message, 'Lỗi');
                }
            });
        }
    }

    chooseFile(file: IFileStorage) {
        const fileIndex: number = this.fileList.findIndex(v => v.name === file.name);
        if (fileIndex >= 0) {
            if (this.fileList[fileIndex].checked) {
                this.fileList[fileIndex].checked = false;
                if (!this.multiple) {
                    this.fileList.forEach((v: any, i: number): void => {
                        v.disable = false;
                    })
                }
            } else {
                if (!this.fileList[fileIndex].disable) {
                    this.fileList[fileIndex].checked = true;
                    if (!this.multiple) {
                        this.fileList.forEach((v: any, i: number): void => {
                            if (i !== fileIndex) v.disable = true;
                        })
                    }
                }
            }
        }
    }

    onSubmitChoosedFile(): void {
        const fileChosenResult: IFileStorage[] = this.fileList.filter((v: IFileForChoose) => v.checked);
        this.nbWindowRef.close(fileChosenResult);
    }

    private findFolderRoot(): void {
        this.isLoading = true;
        this.fileFolderService.viewFolder({}).pipe(
            takeUntil(this._destroy$)
        ).subscribe({
            next: (data: IFileFolderListResponse): void => {
                this.isLoading = false;
                if (data) this.fileFolder = data;
                if (data && data?.folderList) this.folderList = data?.folderList;
                if (data && data?.fileList) {
                    this.fileList = data?.fileList.map(v => {
                        return {
                            ...v,
                            checked: false,
                            disable: false
                        }
                    });
                }
                this.updateBreadcrumb(null);
            },
            error: (): void => {
                this.isLoading = false;
            }
        });
    }

    private updateBreadcrumb(folder: IFileFolder | null): void {
        if (folder) {
            const indexBreadcrumbDup: number = this.breadcrumbs.findIndex(v => v.id === folder.id);
            if (indexBreadcrumbDup >= 0) {
                this.breadcrumbs.splice(indexBreadcrumbDup + 1);
            } else {
                this.breadcrumbs.push(folder);
            }
        } else {
            this.breadcrumbs = [];
        }
    }
}
