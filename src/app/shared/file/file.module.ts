import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FileUploadComponent} from './file-upload/file-upload.component';
import {NbCardModule, NbCheckboxModule, NbFormFieldModule, NbIconModule, NbInputModule} from "@nebular/theme";
import {ImageModule} from "@shared/image/image.module";
import {DirectiveModule} from "@core/directive/directive.module";
import {AddFolderWindowComponent} from './component/add-folder-window/add-folder-window.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    FileUploadComponent,
    AddFolderWindowComponent
  ],
  imports: [
    CommonModule,
    NbCardModule,
    ImageModule,
    NbCheckboxModule,
    DirectiveModule,
    FormsModule,
    ReactiveFormsModule,
    NbFormFieldModule,
    NbInputModule,
    NbIconModule
  ],
  exports: [
    FileUploadComponent,
    AddFolderWindowComponent
  ],
})
export class FileModule {
}
