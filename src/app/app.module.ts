import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbThemeModule,
  NbToastrModule,
  NbWindowModule
} from "@nebular/theme";
import {LayoutModule} from "@layout/layout.module";
import {NbEvaIconsModule} from "@nebular/eva-icons";
import {APP_BASE_HREF} from "@angular/common";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CoreModule} from "@core/module/core.module";

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        NbThemeModule.forRoot({name: 'default'}),
        NbSidebarModule.forRoot(),
        NbMenuModule.forRoot(),
        NbToastrModule.forRoot(),
        NbWindowModule.forRoot(),
        NbDialogModule.forRoot(),
        NbEvaIconsModule,
        LayoutModule,
        CoreModule
    ],
    providers: [{
        provide: APP_BASE_HREF,
        useValue: '/'
    }],
    bootstrap: [AppComponent]
})
export class AppModule {
}
