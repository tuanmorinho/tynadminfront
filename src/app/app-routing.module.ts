import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ROUTING_TYN} from "@core/constants/routing-constant";
import {MainComponent} from "@layout/main/main.component";
import {authGuard} from "@core/guard/auth.guard";

const routes: Routes = [
  {
    path: ROUTING_TYN.DEFAULT,
    component: MainComponent,
    data: {
      breadcrumb: 'Trang chủ'
    },
    children: [
      {
        path: ROUTING_TYN.DEFAULT,
        canActivate: [authGuard],
        canActivateChild: [authGuard],
        loadChildren: () => import('./views/pages/entry-page/entry-page.module').then(m => m.EntryPageModule),
        data: { layout: 'main' }
      },
      {
        path: ROUTING_TYN.AUTH_PAGE,
        loadChildren: () => import('./views/pages/auth-page/auth-page.module').then(m => m.AuthPageModule),
        data: { layout: 'empty' }
      },
      {
        path: ROUTING_TYN.SURVEY_PAGE,
        canActivate: [authGuard],
        canActivateChild: [authGuard],
        loadChildren: () => import('./views/pages/survey-page/survey-page.module').then(m => m.SurveyPageModule),
        data: { breadcrumb: 'Quản lý bộ khảo sát', layout: 'main' },
      }
    ]
  },
  {
    path: 'error',
    loadChildren: () => import('./views/pages/error-page/error-page.module').then(m => m.ErrorPageModule)
  },
  {
    path: '**', redirectTo: 'error', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
