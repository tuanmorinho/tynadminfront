import {Component, inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {NbToastrService} from "@nebular/theme";
import {BaseComponent} from "@core/module/base/base.component";
import {SurveyCategoryService, SurveyService} from "@core/services/feature";
import {ICellImage, ICellStatusChange, IFilterModel, IPaginatorState, IStatusInfo} from "@core/model/core";
import {Row} from "ng2-smart-table/lib/lib/data-set/row";
import {ISurvey, SURVEY_STATUS_LIST} from "@core/model/feature";
import {ROUTING_CONST, ROUTING_SURVEY_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";
import {of} from "rxjs";
import {CellImageComponent} from "@shared/table/components/cell-image/cell-image.component";
import {CellStatusChangeComponent} from "@shared/table/components/cell-status-change/cell-status-change.component";

@Component({
    selector: 'app-survey-list',
    templateUrl: './survey-list.component.html',
    styleUrls: ['./survey-list.component.scss']
})
export class SurveyListComponent extends BaseComponent implements OnInit {

    filterForm!: FormGroup;
    statusInfo!: IStatusInfo;

    private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
    private toastrService: NbToastrService = inject(NbToastrService);
    private surveyCategoryService: SurveyCategoryService = inject<SurveyCategoryService>(SurveyCategoryService);
    public surveyService: SurveyService = inject<SurveyService>(SurveyService);

    tableSettings: any = {
        actions: {
            edit: true,
            delete: true,
            columnTitle: null,
            position: 'left'
        }
    }

    columns: any = {
        id: {
            title: 'ID',
            width: '20px',
        },
        name: {
            title: 'Tên khảo sát',
        },
        imageThumbnail: {
            title: 'Ảnh thumbnail',
            type: 'custom',
            width: '78px',
            valuePrepareFunction: (value: any): ICellImage => {
                return {image: of(this.baseUrlImage + value), options: of({width: '150', height: 'auto'})}
            },
            renderComponent: CellImageComponent
        },
        imageFeature: {
            title: 'Ảnh feature',
            type: 'custom',
            width: '78px',
            valuePrepareFunction: (value: any): ICellImage => {
                return {image: of(this.baseUrlImage + value), options: of({width: '150', height: 'auto'})}
            },
            renderComponent: CellImageComponent
        },
        status: {
            title: 'Trạng thái',
            type: 'custom',
            width: '200px',
            valuePrepareFunction: (value: number): ICellStatusChange => {
                return {
                    data: value,
                    select: {
                        options: SURVEY_STATUS_LIST,
                        bindLabel: 'label',
                        bindValue: 'value'
                    }
                }
            },
            onComponentInitFunction: (instance: any): void => {
                instance.change.subscribe({
                    next: (dataChange: any): void => {
                        this.updateStatus(dataChange);
                    }
                })
            },
            renderComponent: CellStatusChangeComponent
        }
    }

    filterConfig: IFilterModel[] = [
        {
            type: 'input',
            label: 'Từ khóa',
            placeholder: 'Nhập tên khảo sát để tìm kiếm',
            inputType: 'text',
            formControlName: 'keyword',
        },
        {
            type: 'select_observable',
            label: 'Danh mục',
            placeholder: 'Chọn danh mục khảo sát',
            itemObservable: this.surveyCategoryService.items$,
            bindLabel: 'name',
            bindValue: 'id',
            formControlName: 'surveyCategoryId',
        },
    ];

    ngOnInit(): void {
        this.filterForm = this.fb.group({
            keyword: [null],
            surveyCategoryId: [null],
            status: [null],
            isTrash: [false]
        });
        this.find(null);
        this.surveyCategoryService.setDefaultState();
        this.surveyCategoryService.patchState({
            filter: {
                page: 1,
                take: 500
            }
        }, true);
    }

    override get formControls() {
        return this.filterForm.controls;
    }

    find(filter: { status: number | null, isTrash: boolean } | null): void {
        this.surveyService.statusInfo().subscribe({
            next: (statusInfo: IStatusInfo): void => {
                this.statusInfo = statusInfo;
                this.tableSettings = {
                    ...this.tableSettings,
                    actions: {
                        edit: !filter?.isTrash,
                        columnTitle: null,
                        position: 'left'
                    },
                    delete: {
                        deleteButtonContent: !filter?.isTrash ? '<img src="../../../assets/svg/trash-2.svg" width="25" height="25" alt="_"/>' : '<img src="../../../assets/svg/refresh.svg" width="25" height="25" alt="_"/>'
                    }
                }
                if (filter) {
                    this.filterForm.patchValue(filter);
                }
                this.surveyService.setDefaultState();
                this.surveyService.patchState({filter: this.filterForm.value}, filter !== null ?? true);
            }
        });
    }

    pagination(paginator: Partial<IPaginatorState>): void {
        this.surveyService.patchState({filter: paginator});
    }

    search(value: any): void {
        this.surveyService.patchState({
            filter: value
        });
    }

    reset(): void {
        this.filterForm.reset();
        this.filterForm.patchValue({isTrash: false});
        this.surveyService.setDefaultState();
        this.surveyService.patchState({filter: this.filterForm.value});
    }

    toEdit(row?: Row): void {
        if (row && row.getData() && (row.getData() as ISurvey).id) {
            void this.router.navigate([`${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY}/${ROUTING_CONST.DETAIL}/${(row.getData() as ISurvey).id}`])
        }

        if (!row) {
            void this.router.navigate([`${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY}/${ROUTING_CONST.ADD}`])
        }
    }

    delete(row?: Row): void {
        if (this.formControls['isTrash'].value) {
            if (confirm("Bạn có chắc chắn muốn khôi phục?")) {
                this.surveyService.restore(row?.getData() as ISurvey).subscribe({
                    next: (): void => {
                        this.toastrService.success("Khôi phục từ thùng rác thành công", 'Thành công');
                        this.find({status: null, isTrash: true});
                    },
                    error: () => this.toastrService.danger("Có lỗi khi khôi phục từ thùng rác", 'Lỗi')
                });
            }
        } else {
            if (confirm("Bạn có chắc chắn muốn bỏ vào thùng rác?")) {
                this.surveyService.trash(row?.getData() as ISurvey).subscribe({
                    next: (): void => {
                        this.toastrService.success("Bỏ vào thùng rác thành công", 'Thành công');
                        this.find({status: this.formControls['status'].value, isTrash: false});
                    },
                    error: () => this.toastrService.danger("Có lỗi khi bỏ vào thùng rác", 'Lỗi')
                });
            }
        }
    }

    updateStatus(dataChange: any): void {
        this.surveyService.get(dataChange.row.id).subscribe({
            next: (survey: ISurvey): void => {
                if (survey.status !== dataChange.value) {
                    if (confirm('Bạn có chắc chắn muốn cập nhật trạng thái?')) {
                        survey.status = dataChange.value;
                        this.surveyService.updateStatus(survey).subscribe({
                            next: (): void => {
                                this.toastrService.success('Cập nhật trạng thái thành công', 'Thành công');
                                this.find({
                                    status: this.formControls['status'].value,
                                    isTrash: this.formControls['isTrash'].value
                                });
                            },
                            error: (error): void => {
                                this.toastrService.danger(error.error.message, 'Lỗi');
                                this.find({
                                    status: this.formControls['status'].value,
                                    isTrash: this.formControls['isTrash'].value
                                });
                            }
                        });
                    } else {
                        this.find({
                            status: this.formControls['status'].value,
                            isTrash: this.formControls['isTrash'].value
                        });
                    }
                }
            }
        });
    }
}
