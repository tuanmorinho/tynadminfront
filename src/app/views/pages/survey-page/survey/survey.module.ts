import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyListComponent } from './survey-list/survey-list.component';
import { SurveyDetailComponent } from './survey-detail/survey-detail.component';
import {RouterModule, Routes} from "@angular/router";
import {DirectiveModule} from "@core/directive/directive.module";
import {FilterModule} from "@shared/filter/filter.module";
import {NbButtonModule, NbCardModule, NbFormFieldModule, NbInputModule, NbSelectModule} from "@nebular/theme";
import {TableModule} from "@shared/table/table.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "@shared/shared.module";
import {ROUTING_CONST} from "@core/constants/routing-constant";

const routes: Routes = [
  {
    path: ROUTING_CONST.DEFAULT,
    data: {breadcrumb: 'Khảo sát'},
    children: [
      {
        path: ROUTING_CONST.LIST,
        pathMatch: 'full',
        component: SurveyListComponent,
      },
      {
        path: ROUTING_CONST.ADD,
        pathMatch: 'full',
        component: SurveyDetailComponent,
        data: {breadcrumb: 'Thêm mới khảo sát'}
      },
      {
        path: ROUTING_CONST.DETAIL + "/:id",
        pathMatch: 'full',
        component: SurveyDetailComponent,
        data: {breadcrumb: 'Chi tiết khảo sát'}
      },
      {
        path: '**', redirectTo: ROUTING_CONST.LIST
      }
    ]
  },
];

@NgModule({
  declarations: [
    SurveyListComponent,
    SurveyDetailComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    DirectiveModule,
    FilterModule,
    NbButtonModule,
    NbCardModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
    NbFormFieldModule,
    NbInputModule,
    SharedModule,
    NbSelectModule
  ]
})
export class SurveyModule { }
