import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {
  ISurvey,
  ISurveyCategory,
  ISurveyRequest,
  SURVEY_NEXT_STEP_TYPE,
  SURVEY_NEXT_STEP_TYPE_LIST,
  SURVEY_STATUS
} from "@core/model/feature";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder, Validators} from "@angular/forms";
import {NbToastrService} from "@nebular/theme";
import {SurveyCategoryService, SurveyService} from "@core/services/feature";
import {ROUTING_CONST, ROUTING_SURVEY_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";
import {Observable} from "rxjs";

@Component({
  selector: 'app-survey-detail',
  templateUrl: './survey-detail.component.html',
  styleUrls: ['./survey-detail.component.scss']
})
export class SurveyDetailComponent extends BaseComponent implements OnInit {

  surveyId!: number | null;
  survey!: ISurvey;

  private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);
  private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
  private toastrService: NbToastrService = inject<NbToastrService>(NbToastrService);
  private surveyService: SurveyService = inject<SurveyService>(SurveyService);
  private surveyCategoryService: SurveyCategoryService = inject<SurveyCategoryService>(SurveyCategoryService);

  readonly surveyCategoryList$: Observable<ISurveyCategory[]> = this.surveyCategoryService.items$;
  readonly SURVEY_NEXT_STEP_TYPE_LIST = SURVEY_NEXT_STEP_TYPE_LIST;
  readonly SURVEY_NEXT_STEP_TYPE = SURVEY_NEXT_STEP_TYPE;
  readonly SURVEY_STATUS = SURVEY_STATUS;

  ngOnInit(): void {
    this.form = this.fb.group({
      id: null,
      surveyCategoryId: [null, Validators.required],
      code: null,
      name: [null, Validators.compose([Validators.required, Validators.maxLength(256)])],
      slug: null,
      description: [null, Validators.maxLength(4000)],
      imageThumbnail: [null, Validators.maxLength(4000)],
      imageFeature: [null, Validators.maxLength(4000)],
      nextStepType: [{value: SURVEY_NEXT_STEP_TYPE.NEXT_STEP_TYPE_TREE.value, disabled: true}, Validators.required],
      status: null,
      isTrash: null,
    });

    this.surveyId = Number(this.route.snapshot.paramMap.get('id'));
    if (this.surveyId) {
      this.surveyService.detailSurvey(this.surveyId).subscribe({
        next: (data: ISurvey): void => {
          this.form.patchValue(data);
        }
      })
    }

    this.surveyCategoryService.patchState({
      filter: {
        page: 1,
        take: 500
      }
    }, true);
  }

  changeImageThumbnail(imageUrl: string): void {
    this.formControls['imageThumbnail'].setValue(imageUrl);
  }

  deleteImageThumbnail(): void {
    this.formControls['imageThumbnail'].setValue(null);
  }

  changeImageFeature(imageUrl: string): void {
    this.formControls['imageFeature'].setValue(imageUrl);
  }

  deleteImageFeature(): void {
    this.formControls['imageFeature'].setValue(null);
  }

  back(): void {
    void this.router.navigate([`/${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY}/${ROUTING_CONST.LIST}`]);
  }

  onSubmit(): void {
    this.form.markAllAsTouched();
    if (this.form.invalid) return;
    if (this.surveyId) {
      this.update({...this.form.getRawValue()});
    } else {
      this.insert({...this.form.getRawValue()});
    }
  }

  private insert(survey: ISurveyRequest): void {
    this.surveyService.createSurvey(survey).subscribe({
      next: (): void => {
        this.toastrService.success('Thêm mới thành công', 'Thành công');
        this.back();
      },
      error: (error): void => {
        this.toastrService.danger(error.error.message, 'Lỗi');
      }
    });
  }

  private update(survey: ISurveyRequest): void {
    this.surveyService.updateSurvey(survey).subscribe({
      next: (): void => {
        this.toastrService.success('Cập nhập thành công', 'Thành công');
        this.back();
      },
      error: (error): void => {
        this.toastrService.danger(error.error.message, 'Lỗi');
      }
    });
  }
}
