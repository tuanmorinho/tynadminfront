import {Component, inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NbToastrService} from "@nebular/theme";
import {SurveyQuestionService, SurveyService} from "@core/services/feature";
import {IFilterModel, IPaginatorState} from "@core/model/core";
import {Row} from "ng2-smart-table/lib/lib/data-set/row";
import {ISurveyQuestion, ISurveyQuestionRequest, SURVEY_QUESTION_ANSWER_DISPLAY_TYPE_LIST} from "@core/model/feature";
import {ROUTING_CONST, ROUTING_SURVEY_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";
import {BaseComponent} from "@core/module/base/base.component";
import {CellCheckboxComponent} from "@shared/table/components/cell-checkbox/cell-checkbox.component";
import {ActivatedRoute} from "@angular/router";
import {CellCustomComponent} from "@shared/table/components/cell-custom/cell-custom.component";

@Component({
    selector: 'app-survey-question-list',
    templateUrl: './survey-question-list.component.html',
    styleUrls: ['./survey-question-list.component.scss']
})
export class SurveyQuestionListComponent extends BaseComponent {

    filterForm!: FormGroup;
    canCreateSurveyQuestion: boolean = false;

    private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
    private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);
    private toastrService: NbToastrService = inject(NbToastrService);
    private surveyService: SurveyService = inject<SurveyService>(SurveyService);
    public surveyQuestionService: SurveyQuestionService = inject<SurveyQuestionService>(SurveyQuestionService);

    columns: any = {
        id: {
            title: 'ID',
            width: '20px',
        },
        name: {
            title: 'Tên câu hỏi',
        },
        isAnswerRequired: {
            title: 'Yêu cầu trả lời',
            type: 'custom',
            width: '60px',
            valuePrepareFunction: (value: any): any => {
                return {data: value}
            },
            renderComponent: CellCheckboxComponent
        },
        isFirst: {
            title: 'Là câu đầu tiên',
            type: 'custom',
            width: '60px',
            valuePrepareFunction: (value: any): any => {
                return {data: value}
            },
            renderComponent: CellCheckboxComponent
        },
        answerDisplayType: {
            title: 'Hiển thị câu trả lời',
            type: 'custom',
            width: '200px',
            valuePrepareFunction: (value: any): any => {
                return {text: SURVEY_QUESTION_ANSWER_DISPLAY_TYPE_LIST.find(v => v.value === value)?.label}
            },
            renderComponent: CellCustomComponent
        },
        answerSelectType: {
            title: 'Chọn câu trả lời',
            type: 'custom',
            width: '200px',
            valuePrepareFunction: (value: any): any => {
                return {text: SURVEY_QUESTION_ANSWER_DISPLAY_TYPE_LIST.find(v => v.value === value)?.label}
            },
            renderComponent: CellCustomComponent
        }
    }

    filterConfig: IFilterModel[] = [
        {
            type: 'select_observable',
            label: 'Khảo sát',
            placeholder: 'Chọn khảo sát',
            itemObservable: this.surveyService.items$,
            bindLabel: 'name',
            bindValue: 'id',
            formControlName: 'surveyId',
            isRequired: true
        }
    ];

    ngOnInit(): void {
        this.filterForm = this.fb.group({
            surveyId: [null, Validators.required],
        });

        this.surveyService.setDefaultState();
        this.surveyService.patchState({
            filter: {
                page: 1,
                take: 500
            }
        }, true);

        this.reset();

        if (this.route.snapshot.queryParams['surveyId']) {
            this.filterForm.controls['surveyId'].setValue(Number(this.route.snapshot.queryParams['surveyId']));
            this.search({surveyId: Number(this.route.snapshot.queryParams['surveyId'])});
        }

        this.validateCreateSurveyQuestionBtn();
    }

    validateCreateSurveyQuestionBtn(): void {
        this.surveyQuestionService.validateCreateSurveyQuestion({
            surveyId: Number(this.route.snapshot.queryParams['surveyId'])
        }).subscribe({
            next: (data: boolean) => this.canCreateSurveyQuestion = data
        });
    }

    pagination(paginator: Partial<IPaginatorState>): void {
        this.surveyQuestionService.patchState({filter: {...this.filterForm.value, ...paginator}});
    }

    search(value: any): void {
        this.filterForm.markAllAsTouched();
        if (this.filterForm.invalid) {
            this.toastrService.warning('Chọn bộ lọc để tìm kiếm', 'Nhắc nhở');
            return;
        }
        this.surveyQuestionService.patchState({
            filter: value
        });
        this.router.navigate(
            ['.'],
            {
                relativeTo: this.route,
                queryParams: {surveyId: value?.surveyId}
            }
        ).then(() => this.validateCreateSurveyQuestionBtn());
    }

    reset(): void {
        this.router.navigate(
            ['.'],
            {
                relativeTo: this.route,
                queryParams: {surveyId: null}
            }
        ).then(() => this.validateCreateSurveyQuestionBtn());
        this.filterForm.reset();
        this.surveyQuestionService.setDefaultState();
        this.surveyQuestionService.items = [];
    }

    toEdit(row?: Row): void {
        if (row && row.getData() && (row.getData() as ISurveyQuestion).id) {
            void this.router.navigate([`${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_QUESTION}/${ROUTING_CONST.DETAIL}/${(row.getData() as ISurveyQuestion).id}`])
        }

        if (!row) {
            void this.router.navigate([`${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_QUESTION}/${ROUTING_CONST.ADD}`], {
                queryParams: {surveyId: this.filterForm.controls['surveyId'].value}
            })
        }
    }

    delete(row?: Row): void {
        if (confirm("Bạn có chắc chắn muốn xóa?")) {
            this.surveyQuestionService.deleteSurveyQuestion(row?.getData() as ISurveyQuestionRequest).subscribe({
                next: (): void => {
                    this.toastrService.success("Xóa thành công", 'Thành công');
                    this.search({surveyId: Number(this.route.snapshot.queryParams['surveyId'])});
                },
                error: () => this.toastrService.danger("Có lỗi khi xóa", 'Lỗi')
            });
        }
    }
}
