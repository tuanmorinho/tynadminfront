import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {
    ISurvey,
    ISurveyQuestion,
    ISurveyQuestionRequest,
    SURVEY_QUESTION_ANSWER_DISPLAY_TYPE,
    SURVEY_QUESTION_ANSWER_DISPLAY_TYPE_LIST,
    SURVEY_QUESTION_ANSWER_SELECT_TYPE,
    SURVEY_QUESTION_ANSWER_SELECT_TYPE_LIST,
    SURVEY_STATUS
} from "@core/model/feature";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder, Validators} from "@angular/forms";
import {NbToastrService} from "@nebular/theme";
import {SurveyQuestionService, SurveyService} from "@core/services/feature";
import {distinctUntilChanged, Observable, takeUntil} from "rxjs";
import {ROUTING_CONST, ROUTING_SURVEY_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";
import {editorConfig} from "@core/constants/editor-config";
import {AngularEditorConfig} from "@kolkov/angular-editor";

@Component({
    selector: 'app-survey-question-detail',
    templateUrl: './survey-question-detail.component.html',
    styleUrls: ['./survey-question-detail.component.scss']
})
export class SurveyQuestionDetailComponent extends BaseComponent implements OnInit {

    surveyId!: number | undefined;
    surveyQuestionId!: number | null;
    surveyQuestion!: ISurveyQuestion;

    private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);
    private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
    private toastrService: NbToastrService = inject<NbToastrService>(NbToastrService);
    private surveyQuestionService: SurveyQuestionService = inject<SurveyQuestionService>(SurveyQuestionService);
    private surveyService: SurveyService = inject<SurveyService>(SurveyService);

    readonly editorConfig: AngularEditorConfig = editorConfig;
    readonly surveyList$: Observable<ISurvey[]> = this.surveyService.items$;
    readonly SURVEY_QUESTION_ANSWER_DISPLAY_TYPE_LIST = SURVEY_QUESTION_ANSWER_DISPLAY_TYPE_LIST;
    readonly SURVEY_QUESTION_ANSWER_SELECT_TYPE_LIST = SURVEY_QUESTION_ANSWER_SELECT_TYPE_LIST;

    ngOnInit(): void {
        this.form = this.fb.group({
            id: null,
            surveyId: [null, Validators.required],
            name: [null, Validators.compose([Validators.required, Validators.maxLength(256)])],
            description: [null, Validators.maxLength(4000)],
            imageThumbnail: [null, Validators.maxLength(4000)],
            imageFeature: [null, Validators.maxLength(4000)],
            isAnswerRequired: [null],
            isFirst: [null],
            answerSelectType: [SURVEY_QUESTION_ANSWER_SELECT_TYPE.ANSWER_SELECT_TYPE_ONE, Validators.required],
            answerDisplayType: [SURVEY_QUESTION_ANSWER_DISPLAY_TYPE.ANSWER_DISPLAY_TYPE_LIST, Validators.required]
        });

        this.surveyQuestionId = Number(this.route.snapshot.paramMap.get('id'));
        if (this.surveyQuestionId) {
            this.surveyQuestionService.detailSurveyQuestion(this.surveyQuestionId).subscribe({
                next: (data: ISurveyQuestion): void => {
                    this.form.patchValue(data);
                }
            })
        }

        this.surveyService.setDefaultState();
        this.surveyService.patchState({
            filter: {
                page: 1,
                take: 500,
                isTrash: false,
                status: SURVEY_STATUS.STATUS_INACTIVE.value
            }
        }, true);
        this.surveyService.items$.pipe(
            distinctUntilChanged((a: ISurvey[], b: ISurvey[]): boolean => JSON.stringify(a) === JSON.stringify(b)),
            takeUntil(this._destroy$)
        ).subscribe({
            next: (surveys: ISurvey[]): void => {
                if (surveys.length > 0) {
                    if (this.route.snapshot.queryParams['surveyId'] && !this.surveyQuestionId) {
                        this.surveyId = this.route.snapshot.queryParams['surveyId'];
                        this.formControls['surveyId'].setValue(Number(this.route.snapshot.queryParams['surveyId']));
                    }
                }
            }
        });
    }

    changeImageThumbnail(imageUrl: string): void {
        this.formControls['imageThumbnail'].setValue(imageUrl);
    }

    deleteImageThumbnail(): void {
        this.formControls['imageThumbnail'].setValue(null);
    }

    changeImageFeature(imageUrl: string): void {
        this.formControls['imageFeature'].setValue(imageUrl);
    }

    deleteImageFeature(): void {
        this.formControls['imageFeature'].setValue(null);
    }

    back(surveyId?: number): void {
        if (surveyId) {
            void this.router.navigate([`/${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_QUESTION}/${ROUTING_CONST.LIST}`], {
                queryParams: {
                    surveyId
                }
            });
        } else void this.router.navigate([`/${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_QUESTION}/${ROUTING_CONST.LIST}`]);
    }

    onSubmit(): void {
        this.form.markAllAsTouched();
        if (this.form.invalid) return;
        if (this.surveyQuestionId) {
            this.update({...this.form.getRawValue()});
        } else {
            this.insert({...this.form.getRawValue()});
        }
    }

    private insert(surveyQuestion: ISurveyQuestionRequest): void {
        this.surveyQuestionService.createSurveyQuestion(surveyQuestion).subscribe({
            next: (): void => {
                this.toastrService.success('Thêm mới thành công', 'Thành công');
                this.back(surveyQuestion.surveyId);
            },
            error: (error): void => {
                this.toastrService.danger(error.error.message, 'Lỗi');
            }
        });
    }

    private update(surveyQuestion: ISurveyQuestionRequest): void {
        this.surveyQuestionService.updateSurveyQuestion(surveyQuestion).subscribe({
            next: (): void => {
                this.toastrService.success('Cập nhập thành công', 'Thành công');
                this.back(surveyQuestion.surveyId);
            },
            error: (error): void => {
                this.toastrService.danger(error.error.message, 'Lỗi');
            }
        });
    }
}
