import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {SurveyQuestionListComponent} from './survey-question-list/survey-question-list.component';
import {SurveyQuestionDetailComponent} from './survey-question-detail/survey-question-detail.component';
import {DirectiveModule} from "@core/directive/directive.module";
import {FilterModule} from "@shared/filter/filter.module";
import {
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbFormFieldModule,
    NbInputModule,
    NbOptionModule,
    NbSelectModule
} from "@nebular/theme";
import {TableModule} from "@shared/table/table.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "@shared/shared.module";
import {ROUTING_CONST} from "@core/constants/routing-constant";
import {AngularEditorModule} from "@kolkov/angular-editor";
import {surveyQuestionAddGuard} from "@core/guard/survey-question-add.guard";

const routes: Routes = [
    {
        path: ROUTING_CONST.DEFAULT,
        data: {breadcrumb: 'Câu hỏi khảo sát'},
        children: [
            {
                path: ROUTING_CONST.LIST,
                pathMatch: 'full',
                component: SurveyQuestionListComponent,
            },
            {
                path: ROUTING_CONST.ADD,
                pathMatch: 'full',
                canActivate: [surveyQuestionAddGuard],
                component: SurveyQuestionDetailComponent,
                data: {breadcrumb: 'Thêm mới câu hỏi'}
            },
            {
                path: ROUTING_CONST.DETAIL + "/:id",
                pathMatch: 'full',
                component: SurveyQuestionDetailComponent,
                data: {breadcrumb: 'Chi tiết câu hỏi'}
            },
            {
                path: '**', redirectTo: ROUTING_CONST.LIST
            }
        ]
    },
];

@NgModule({
    declarations: [
        SurveyQuestionListComponent,
        SurveyQuestionDetailComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        DirectiveModule,
        FilterModule,
        NbButtonModule,
        NbCardModule,
        TableModule,
        FormsModule,
        ReactiveFormsModule,
        NbFormFieldModule,
        NbInputModule,
        SharedModule,
        NbOptionModule,
        NbSelectModule,
        AngularEditorModule,
        NbCheckboxModule
    ]
})
export class SurveyQuestionModule {
}
