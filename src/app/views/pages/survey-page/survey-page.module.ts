import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "@shared/shared.module";
import {CoreModule} from "@core/module/core.module";
import {NbAlertModule, NbDialogModule, NbFormFieldModule, NbInputModule} from "@nebular/theme";
import {ROUTING_SURVEY_PAGE} from "@core/constants/routing-constant";

const routes: Routes = [
    {
        path: ROUTING_SURVEY_PAGE.SURVEY_CATEGORY,
        loadChildren: () => import('./survey-category/survey-category.module').then(m => m.SurveyCategoryModule),
        data: {breadcrumb: null}
    },
    {
        path: ROUTING_SURVEY_PAGE.SURVEY,
        loadChildren: () => import('./survey/survey.module').then(m => m.SurveyModule),
        data: {breadcrumb: null}
    },
    {
        path: ROUTING_SURVEY_PAGE.SURVEY_QUESTION,
        loadChildren: () => import('./survey-question/survey-question.module').then(m => m.SurveyQuestionModule),
        data: {breadcrumb: null}
    },
    {
        path: ROUTING_SURVEY_PAGE.SURVEY_ANSWER,
        loadChildren: () => import('./survey-answer/survey-answer.module').then(m => m.SurveyAnswerModule),
        data: {breadcrumb: null}
    },
    {
        path: ROUTING_SURVEY_PAGE.SURVEY_QUESTION_STEP,
        loadChildren: () => import('./survey-question-step/survey-question-step.module').then(m => m.SurveyQuestionStepModule),
        data: {breadcrumb: null}
    }
]

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        CoreModule,
        NbInputModule,
        NbAlertModule,
        NbFormFieldModule,
        NbDialogModule.forChild()
    ]
})
export class SurveyPageModule {
}
