import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {NbDialogRef, NbDialogService, NbToastrService} from "@nebular/theme";
import {IFilterModel} from "@core/model/core";
import {
    SurveyAnswerService,
    SurveyQuestionStepService,
    SurveyQuestionService,
    SurveyService
} from "@core/services/feature";
import {Observable, takeUntil} from "rxjs";
import {ISurvey, ISurveyQuestionStep} from "@core/model/feature";
import {
    SurveyQuestionStepConfigComponent
} from "@pages/survey-page/survey-question-step/components/survey-question-step-config/survey-question-step-config.component";

@Component({
    selector: 'app-step-mapping-qa',
    templateUrl: './step-mapping-qa.component.html',
    styleUrls: ['./step-mapping-qa.component.scss']
})
export class StepMappingQAComponent extends BaseComponent implements OnInit {

    filterForm!: FormGroup;
    survey!: ISurvey | null;
    surveyQuestionStepList: ISurveyQuestionStep[] = [];

    private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
    private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);
    private toastrService: NbToastrService = inject(NbToastrService);
    private dialogService: NbDialogService = inject<NbDialogService>(NbDialogService);
    private surveyService: SurveyService = inject<SurveyService>(SurveyService);
    private surveyQuestionService: SurveyQuestionService = inject<SurveyQuestionService>(SurveyQuestionService);
    private surveyAnswerService: SurveyAnswerService = inject<SurveyAnswerService>(SurveyAnswerService);
    private surveyQuestionStepService: SurveyQuestionStepService = inject<SurveyQuestionStepService>(SurveyQuestionStepService);

    readonly surveyList$: Observable<ISurvey[]> = this.surveyService.items$;

    filterConfig: IFilterModel[] = [
        {
            type: 'select_observable',
            label: 'Khảo sát',
            placeholder: 'Chọn khảo sát',
            itemObservable: this.surveyService.items$,
            bindLabel: 'name',
            bindValue: 'id',
            formControlName: 'surveyId',
            isRequired: true
        }
    ];

    ngOnInit(): void {
        this.filterForm = this.fb.group({
            surveyId: [null, Validators.required]
        });

        this.form = this.fb.group({
            surveyAnswerId: []
        })

        this.surveyService.setDefaultState();
        this.surveyService.patchState({
            filter: {
                page: 1,
                take: 500
            },
            sorting: [{column: 'id', direction: 'asc'}]
        }, true);

        this.reset();

        if (this.route.snapshot.queryParams['surveyId']) {
            this.filterForm.controls['surveyId'].setValue(Number(this.route.snapshot.queryParams['surveyId']));
            this.search({surveyId: Number(this.route.snapshot.queryParams['surveyId'])});
        }
    }

    search(value: any): void {
        this.filterForm.markAllAsTouched();
        if (this.filterForm.invalid) {
            this.toastrService.warning('Chọn bộ lọc để tìm kiếm', 'Nhắc nhở');
            return;
        }
        this.surveyService.detailSurvey(value?.surveyId).subscribe((survey: ISurvey) => this.survey = survey);
        this.surveyQuestionStepService.detailStepMapping(Number(value?.surveyId))
            .pipe(takeUntil(this._destroy$))
            .subscribe({
                next: (data: ISurveyQuestionStep[]) => this.surveyQuestionStepList = data
            });
        void this.router.navigate(
            ['.'],
            {
                relativeTo: this.route,
                queryParams: {surveyId: value?.surveyId}
            }
        );
    }

    reset(): void {
        void this.router.navigate(
            ['.'],
            {
                relativeTo: this.route,
                queryParams: {surveyId: null}
            }
        );
        this.filterForm.reset();
        this.survey = null;
    }

    configStep(survey: ISurvey): void {
        const dialogRef: NbDialogRef<SurveyQuestionStepConfigComponent> = this.dialogService.open(SurveyQuestionStepConfigComponent, {
            context: {
                surveyId: survey?.id,
            },
            closeOnBackdropClick: false
        });
        dialogRef.onClose.subscribe({
            next: (stepMapping: ISurveyQuestionStep[]): void => {
                if (stepMapping && stepMapping.length) {
                    console.log(stepMapping);
                    this.surveyQuestionStepService.doMappingStep(stepMapping).pipe(
                        takeUntil(this._destroy$)
                    ).subscribe({
                        next: (): void => {
                            this.toastrService.success('Cấu hình thành công', 'Thành công');
                        },
                        error: (error): void => {
                            this.toastrService.danger(error.error.message, 'Lỗi');
                        }
                    })
                }
            }
        })
    }
}
