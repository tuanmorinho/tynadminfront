import {Component, inject, Input, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, Validators} from "@angular/forms";
import {NbDialogRef} from "@nebular/theme";
import {Observable, takeUntil} from "rxjs";
import {ISurveyAnswer, ISurveyQuestion, ISurveyQuestionStep} from "@core/model/feature";
import {SurveyAnswerService, SurveyQuestionService} from "@core/services/feature";
import {BaseComponent} from "@core/module/base/base.component";

@Component({
    selector: 'app-survey-question-step-config',
    templateUrl: './survey-question-step-config.component.html',
    styleUrls: ['./survey-question-step-config.component.scss']
})
export class SurveyQuestionStepConfigComponent extends BaseComponent implements OnInit {
    @Input() surveyId!: number;

    formArray!: FormArray;

    private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
    private dialogRef: NbDialogRef<SurveyQuestionStepConfigComponent> = inject<NbDialogRef<SurveyQuestionStepConfigComponent>>(NbDialogRef);
    private surveyQuestionService: SurveyQuestionService = inject<SurveyQuestionService>(SurveyQuestionService);
    private surveyAnswerService: SurveyAnswerService = inject<SurveyAnswerService>(SurveyAnswerService);

    readonly surveyQuestionList$: Observable<ISurveyQuestion[]> = this.surveyQuestionService.items$;

    ngOnInit(): void {
        this.formArray = this.fb.array([]);

        if (this.surveyId) {
            this.surveyQuestionService.patchState({
                filter: {
                    surveyId: this.surveyId,
                    page: 1,
                    take: 500
                },
                sorting: [{column: 'isFirst', direction: 'desc'}]
            }, true);
        }

        this.addStepQuestion();
        this.addStepAnswer(0);
    }

    selectQuestion(surveyQuestionId: number, stepQuestionIndex: number): void {
        if (surveyQuestionId) {
            this.surveyAnswerService.findAllSurveyAnswer(surveyQuestionId).pipe(
                takeUntil(this._destroy$)
            ).subscribe({
                next: (data: ISurveyAnswer[]): void => {
                    this.stepQuestionControl('surveyAnswer', stepQuestionIndex)?.setValue(data);
                }
            })
        }
    }

    addStepQuestion(): void {
        this.formArray.push(this.fb.group({
            surveyQuestionId: [null, Validators.required],
            surveyAnswer: [null],
            step: this.fb.array([])
        }))
    }

    addStepAnswer(stepQuestionIndex: number): void {
        (this.formArray.at(stepQuestionIndex).get('step') as FormArray).push(this.fb.group({
            surveyAnswerId: [null, Validators.required],
            surveyQuestionNextId: [null, Validators.required]
        }))
    }

    removeStepQuestion(stepQuestionIndex: number): void {
        this.formArray.removeAt(stepQuestionIndex);
    }

    removeStepAnswer(stepQuestionIndex: number, stepAnswerIndex: number): void {
        (this.formArray.at(stepQuestionIndex).get('step') as FormArray).removeAt(stepAnswerIndex);
    }

    stepQuestionControls(): AbstractControl[] {
        return this.formArray.controls;
    }

    stepAnswerControls(stepQuestionIndex: number): AbstractControl[] {
        return (this.formArray.at(stepQuestionIndex).get('step') as FormArray).controls;
    }

    stepQuestionControl(controlName: string, stepQuestionIndex: number): AbstractControl | null {
        return this.formArray.at(stepQuestionIndex).get(controlName);
    }

    stepQuestionControlStatus(controlName: string, stepQuestionIndex: number): boolean {
        return Boolean(this.formArray.at(stepQuestionIndex).get(controlName)?.invalid && this.formArray.at(stepQuestionIndex).get(controlName)?.touched);
    }

    stepAnswerControl(controlName: string, stepQuestionIndex: number, stepAnswerIndex: number): AbstractControl | null {
        return (this.formArray.at(stepQuestionIndex).get('step') as FormArray).at(stepAnswerIndex).get(controlName);
    }

    stepAnswerControlStatus(controlName: string, stepQuestionIndex: number, stepAnswerIndex: number): boolean {
        return Boolean((this.formArray.at(stepQuestionIndex).get('step') as FormArray).at(stepAnswerIndex).get(controlName)?.invalid
            && (this.formArray.at(stepQuestionIndex).get('step') as FormArray).at(stepAnswerIndex).get(controlName)?.touched);
    }

    back(): void {
        this.dialogRef.close();
    }

    onSubmit(): void {
        this.formArray.markAllAsTouched();
        if (this.formArray.invalid) return;
        const stepMapping: ISurveyQuestionStep[] = this.filterCondition(this.formArray.getRawValue());
        this.dialogRef.close(stepMapping);
    }

    private filterCondition(data: ISurveyQuestionStep[]): ISurveyQuestionStep[] {
        data.forEach((v: ISurveyQuestionStep) => {
            if (v.step.length > 0) {
                v.step = Object.values(v.step.reduce((acc, itm) => ({...acc, [itm.surveyAnswerId]: {...itm}}), {}))
            }
        });
        return data;
    }
}
