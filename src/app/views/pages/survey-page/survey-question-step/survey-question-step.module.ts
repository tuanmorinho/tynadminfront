import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StepMappingQAComponent } from './step-mapping-qa/step-mapping-qa.component';
import {RouterModule, Routes} from "@angular/router";
import {DirectiveModule} from "@core/directive/directive.module";
import {SharedModule} from "@shared/shared.module";
import {ROUTING_CONST} from "@core/constants/routing-constant";
import {
    NbButtonModule,
    NbCardModule, NbCheckboxModule,
    NbFormFieldModule, NbIconModule,
    NbInputModule, NbLayoutModule,
    NbOptionModule,
    NbSelectModule, NbSidebarModule, NbTooltipModule
} from "@nebular/theme";
import {FilterModule} from "@shared/filter/filter.module";
import {TableModule} from "@shared/table/table.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AngularEditorModule} from "@kolkov/angular-editor";
import { SurveyQuestionStepConfigComponent } from './components/survey-question-step-config/survey-question-step-config.component';

const routes: Routes = [
  {
    path: ROUTING_CONST.DEFAULT,
    data: {breadcrumb: 'Cấu hình khảo sát rẽ nhánh'},
    children: [
      {
        path: ROUTING_CONST.DEFAULT,
        pathMatch: 'full',
        component: StepMappingQAComponent,
      },
      {
        path: '**', redirectTo: ROUTING_CONST.DEFAULT
      }
    ]
  },
];

@NgModule({
  declarations: [
    StepMappingQAComponent,
    SurveyQuestionStepConfigComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        DirectiveModule,
        FilterModule,
        NbButtonModule,
        NbCardModule,
        TableModule,
        FormsModule,
        ReactiveFormsModule,
        NbFormFieldModule,
        NbInputModule,
        SharedModule,
        NbOptionModule,
        NbSelectModule,
        AngularEditorModule,
        NbCheckboxModule,
        NbLayoutModule,
        NbIconModule,
        NbTooltipModule,
    ]
})
export class SurveyQuestionStepModule { }
