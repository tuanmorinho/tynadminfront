import {Component, inject, OnInit} from '@angular/core';
import {
    ISurvey,
    ISurveyAnswer,
    ISurveyAnswerRequest,
    ISurveyQuestion,
    SURVEY_ANSWER_DISPLAY_TYPE,
    SURVEY_ANSWER_DISPLAY_TYPE_LIST, SURVEY_QUESTION_ANSWER_DISPLAY_TYPE, SURVEY_QUESTION_ANSWER_SELECT_TYPE,
    SURVEY_STATUS
} from "@core/model/feature";
import {BaseComponent} from "@core/module/base/base.component";
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {NbToastrService} from "@nebular/theme";
import {SurveyAnswerService, SurveyQuestionService, SurveyService} from "@core/services/feature";
import {distinctUntilChanged, Observable, takeUntil, throttleTime} from "rxjs";
import {ROUTING_CONST, ROUTING_SURVEY_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";
import {AngularEditorConfig} from "@kolkov/angular-editor";
import {editorConfig} from "@core/constants/editor-config";

@Component({
    selector: 'app-survey-answer-detail',
    templateUrl: './survey-answer-detail.component.html',
    styleUrls: ['./survey-answer-detail.component.scss']
})
export class SurveyAnswerDetailComponent extends BaseComponent implements OnInit {

    surveyId!: number | undefined;
    surveyQuestionId!: number | undefined;
    surveyAnswerId!: number | null;
    surveyAnswer!: ISurveyAnswer;
    surveyQuestionList: ISurveyQuestion[] = [];
    surveyQuestionSelected: ISurveyQuestion | undefined;

    private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);
    private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
    private toastrService: NbToastrService = inject<NbToastrService>(NbToastrService);
    private surveyAnswerService: SurveyAnswerService = inject<SurveyAnswerService>(SurveyAnswerService);
    private surveyQuestionService: SurveyQuestionService = inject<SurveyQuestionService>(SurveyQuestionService);
    private surveyService: SurveyService = inject<SurveyService>(SurveyService);

    readonly editorConfig: AngularEditorConfig = editorConfig;
    readonly surveyList$: Observable<ISurvey[]> = this.surveyService.items$;
    readonly surveyQuestionList$: Observable<ISurveyQuestion[]> = this.surveyQuestionService.items$;
    SURVEY_ANSWER_DISPLAY_TYPE_LIST = SURVEY_ANSWER_DISPLAY_TYPE_LIST;

    ngOnInit(): void {
        this.form = this.fb.group({
            id: null,
            surveyId: [null, Validators.required],
            surveyQuestionId: [null, Validators.required],
            name: [null, Validators.compose([Validators.required, Validators.maxLength(256)])],
            description: [null, Validators.maxLength(4000)],
            imageThumbnail: [null, Validators.maxLength(4000)],
            imageFeature: [null, Validators.maxLength(4000)],
            displayType: [SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_SELECT.value, Validators.required],
            priority: [null],
            demandScore: [1, Validators.compose([Validators.required, Validators.min(1), Validators.max(100)])]
        });

        this.surveyService.setDefaultState();
        this.surveyService.patchState({
            filter: {
                page: 1,
                take: 500,
                isTrash: false,
                status: SURVEY_STATUS.STATUS_INACTIVE.value
            },
            sorting: [{column: 'id', direction: 'asc'}]
        }, true);


        this.surveyQuestionService.items = [];
        this.formControls['surveyId'].valueChanges.pipe(
            distinctUntilChanged(),
            throttleTime(500),
            takeUntil(this._destroy$)
        ).subscribe({
            next: (surveyId: any): void => {
                if (surveyId) {
                    this.formControls['surveyQuestionId'].setValue(null);
                    this.surveyQuestionService.items = [];
                    this.surveyQuestionService.patchState({
                        filter: {
                            page: 1,
                            take: 500,
                            surveyId
                        },
                        sorting: [{column: 'id', direction: 'asc'}]
                    }, true);
                }
            }
        });

        this.surveyAnswerId = Number(this.route.snapshot.paramMap.get('id'));
        if (this.surveyAnswerId) {
            this.surveyAnswerService.detailSurveyAnswer(this.surveyAnswerId).subscribe({
                next: (data: ISurveyAnswer): void => {
                    this.form.patchValue(data);
                }
            })
        }

        this.surveyService.items$.pipe(
            distinctUntilChanged((a: ISurvey[], b: ISurvey[]): boolean => JSON.stringify(a) === JSON.stringify(b)),
            takeUntil(this._destroy$)
        ).subscribe({
            next: (surveys: ISurvey[]): void => {
                if (surveys.length > 0) {
                    if (this.route.snapshot.queryParams['surveyId'] && !this.surveyAnswerId) {
                        this.surveyId = this.route.snapshot.queryParams['surveyId'];
                        this.formControls['surveyId'].setValue(Number(this.route.snapshot.queryParams['surveyId']));
                    }
                }
            }
        })

        this.surveyQuestionService.items$.pipe(
            distinctUntilChanged((a: ISurveyQuestion[], b: ISurveyQuestion[]): boolean => JSON.stringify(a) === JSON.stringify(b)),
            takeUntil(this._destroy$)
        ).subscribe({
            next: (surveyQuestions: ISurveyQuestion[]): void => {
                if (surveyQuestions.length > 0) {
                    this.surveyQuestionList = surveyQuestions;
                    if (this.route.snapshot.queryParams['surveyQuestionId'] && !this.surveyAnswerId) {
                        this.surveyQuestionId = this.route.snapshot.queryParams['surveyQuestionId'];
                        this.formControls['surveyQuestionId'].setValue(Number(this.route.snapshot.queryParams['surveyQuestionId']));
                        this.surveyQuestionSelected = this.surveyQuestionList.find((v: ISurveyQuestion): boolean => v.id === Number(this.surveyQuestionId));
                        if (this.surveyQuestionSelected) {
                            if (this.surveyQuestionSelected.answerSelectType === SURVEY_QUESTION_ANSWER_SELECT_TYPE.ANSWER_SELECT_TYPE_MULTI.value || this.surveyQuestionSelected.answerDisplayType === SURVEY_QUESTION_ANSWER_DISPLAY_TYPE.ANSWER_DISPLAY_TYPE_GRID.value)
                                this.SURVEY_ANSWER_DISPLAY_TYPE_LIST = SURVEY_ANSWER_DISPLAY_TYPE_LIST.filter(v => v.value === SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_SELECT.value);
                        }
                    }
                }
            }
        })
    }

    changeSurveyQuestion(questionId: number): void {
        if (questionId) {
            this.surveyQuestionSelected = this.surveyQuestionList.find((v: ISurveyQuestion): boolean => v.id === questionId);
            if (this.surveyQuestionSelected) {
                if (this.surveyQuestionSelected.answerSelectType === SURVEY_QUESTION_ANSWER_SELECT_TYPE.ANSWER_SELECT_TYPE_MULTI.value || this.surveyQuestionSelected.answerDisplayType === SURVEY_QUESTION_ANSWER_DISPLAY_TYPE.ANSWER_DISPLAY_TYPE_GRID.value)
                    this.SURVEY_ANSWER_DISPLAY_TYPE_LIST = SURVEY_ANSWER_DISPLAY_TYPE_LIST.filter(v => v.value === SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_SELECT.value);
            }
        }
    }

    changeImageThumbnail(imageUrl: string): void {
        this.formControls['imageThumbnail'].setValue(imageUrl);
    }

    deleteImageThumbnail(): void {
        this.formControls['imageThumbnail'].setValue(null);
    }

    changeImageFeature(imageUrl: string): void {
        this.formControls['imageFeature'].setValue(imageUrl);
    }

    deleteImageFeature(): void {
        this.formControls['imageFeature'].setValue(null);
    }

    back(surveyId?: number, surveyQuestionId?: number): void {
        if (surveyId && surveyQuestionId) {
            void this.router.navigate([`/${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_ANSWER}/${ROUTING_CONST.LIST}`], {
                queryParams: {
                    surveyId,
                    surveyQuestionId
                }
            });
        } else void this.router.navigate([`/${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_ANSWER}/${ROUTING_CONST.LIST}`]);
    }

    onSubmit(): void {
        this.form.markAllAsTouched();
        if (this.form.invalid) return;
        if (this.surveyAnswerId) {
            this.update({...this.form.getRawValue()});
        } else {
            this.insert({...this.form.getRawValue()});
        }
    }

    private insert(surveyAnswer: ISurveyAnswerRequest): void {
        this.surveyAnswerService.createSurveyAnswer(surveyAnswer).subscribe({
            next: (): void => {
                this.toastrService.success('Thêm mới thành công', 'Thành công');
                this.back(Number(this.formControls['surveyId'].value), surveyAnswer.surveyQuestionId);
            },
            error: (error): void => {
                this.toastrService.danger(error.error.message, 'Lỗi');
            }
        });
    }

    private update(surveyAnswer: ISurveyAnswerRequest): void {
        this.surveyAnswerService.updateSurveyAnswer(surveyAnswer).subscribe({
            next: (): void => {
                this.toastrService.success('Cập nhập thành công', 'Thành công');
                this.back(Number(this.formControls['surveyId'].value), surveyAnswer.surveyQuestionId);
            },
            error: (error): void => {
                this.toastrService.danger(error.error.message, 'Lỗi');
            }
        });
    }

    protected readonly SURVEY_ANSWER_DISPLAY_TYPE = SURVEY_ANSWER_DISPLAY_TYPE;
}
