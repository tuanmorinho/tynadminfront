import {Component, inject} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {NbDialogRef, NbDialogService, NbToastrService} from "@nebular/theme";
import {SurveyAnswerService, SurveyQuestionService, SurveyService} from "@core/services/feature";
import {ISurveyAnswer, ISurveyAnswerRequest, SURVEY_ANSWER_DISPLAY_TYPE_LIST} from "@core/model/feature";
import {CellCustomComponent} from "@shared/table/components/cell-custom/cell-custom.component";
import {IBasePriorityModel, IFilterModel, IPaginatorState} from "@core/model/core";
import {Row} from "ng2-smart-table/lib/lib/data-set/row";
import {ROUTING_CONST, ROUTING_SURVEY_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";
import {distinctUntilChanged, startWith, takeUntil, throttleTime} from "rxjs";
import {CellPriorityComponent} from "@shared/table/components/cell-priority/cell-priority.component";
import {SortPriorityDialogComponent} from "@shared/components/sort-priority-dialog/sort-priority-dialog.component";

@Component({
    selector: 'app-survey-answer-list',
    templateUrl: './survey-answer-list.component.html',
    styleUrls: ['./survey-answer-list.component.scss']
})
export class SurveyAnswerListComponent extends BaseComponent {

    filterForm!: FormGroup;
    canCreateSurveyAnswer: boolean = false;

    private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
    private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);
    private toastrService: NbToastrService = inject(NbToastrService);
    private dialogService: NbDialogService = inject<NbDialogService>(NbDialogService);
    private surveyService: SurveyService = inject<SurveyService>(SurveyService);
    private surveyQuestionService: SurveyQuestionService = inject<SurveyQuestionService>(SurveyQuestionService);
    public surveyAnswerService: SurveyAnswerService = inject<SurveyAnswerService>(SurveyAnswerService);

    columns: any = {
        priority: {
            title: '',
            type: 'custom',
            width: '25px',
            onComponentInitFunction: (instance: any): void => {
                instance.change.subscribe({
                    next: (): void => {
                        this.sortPriority();
                    }
                })
            },
            renderComponent: CellPriorityComponent
        },
        id: {
            title: 'ID',
            width: '20px',
        },
        name: {
            title: 'Tên câu trả lời',
        },
        displayType: {
            title: 'Kiểu hiển thị',
            type: 'custom',
            valuePrepareFunction: (value: any): any => {
                return {text: SURVEY_ANSWER_DISPLAY_TYPE_LIST.find(v => v.value === value)?.label}
            },
            renderComponent: CellCustomComponent
        }
    }

    filterConfig: IFilterModel[] = [
        {
            type: 'select_observable',
            label: 'Khảo sát',
            placeholder: 'Chọn khảo sát',
            itemObservable: this.surveyService.items$,
            bindLabel: 'name',
            bindValue: 'id',
            formControlName: 'surveyId',
            isRequired: true
        },
        {
            type: 'select_observable',
            label: 'Câu hỏi',
            placeholder: 'Chọn câu hỏi',
            itemObservable: this.surveyQuestionService.items$,
            bindLabel: 'name',
            bindValue: 'id',
            formControlName: 'surveyQuestionId',
            isRequired: true
        }
    ];

    ngOnInit(): void {
        this.filterForm = this.fb.group({
            surveyId: [null, Validators.required],
            surveyQuestionId: [null, Validators.required],
        });

        this.reset();

        if (this.route.snapshot.queryParams['surveyId']) {
            this.filterForm.controls['surveyId'].setValue(Number(this.route.snapshot.queryParams['surveyId']));
        }

        this.surveyService.setDefaultState();
        this.surveyService.patchState({
            filter: {
                page: 1,
                take: 500
            },
            sorting: [{column: 'id', direction: 'asc'}]
        }, true);

        this.surveyQuestionService.items = [];
        this.filterForm.controls['surveyId'].valueChanges.pipe(
            startWith(this.route.snapshot.queryParams['surveyId'] ? Number(this.route.snapshot.queryParams['surveyId']) : null),
            distinctUntilChanged(),
            throttleTime(500),
            takeUntil(this._destroy$)
        ).subscribe({
            next: (surveyId: any): void => {
                if (surveyId) {
                    this.filterForm.controls['surveyQuestionId'].setValue(null);
                    this.surveyQuestionService.items = [];
                    this.surveyQuestionService.patchState({
                        filter: {
                            page: 1,
                            take: 500,
                            surveyId
                        },
                        sorting: [{column: 'id', direction: 'asc'}]
                    }, true);

                    if (this.route.snapshot.queryParams['surveyId'] && this.route.snapshot.queryParams['surveyQuestionId']) {
                        this.filterForm.controls['surveyQuestionId'].setValue(Number(this.route.snapshot.queryParams['surveyQuestionId']));
                        this.search({surveyId: Number(this.route.snapshot.queryParams['surveyId']), surveyQuestionId: Number(this.route.snapshot.queryParams['surveyQuestionId'])});
                    }
                }
            }
        });
        this.validateCreateSurveyAnswerBtn();
    }

    validateCreateSurveyAnswerBtn(): void {
        this.surveyAnswerService.validateCreateSurveyAnswer({
            surveyQuestionId: Number(this.route.snapshot.queryParams['surveyQuestionId']),
            surveyId: Number(this.route.snapshot.queryParams['surveyId'])
        }).subscribe({
            next: (data: boolean) => this.canCreateSurveyAnswer = data
        });
    }

    pagination(paginator: Partial<IPaginatorState>): void {
        this.surveyAnswerService.patchState({filter: paginator, sorting: [{column: 'priority', direction: 'asc'}]});
    }

    search(value: any): void {
        this.filterForm.markAllAsTouched();
        if (this.filterForm.invalid) {
            this.toastrService.warning('Chọn bộ lọc để tìm kiếm', 'Nhắc nhở');
            return;
        }
        this.surveyAnswerService.patchState({
            filter: value,
            sorting: [{column: 'priority', direction: 'asc'}]
        });
        this.router.navigate(
            ['.'],
            {
                relativeTo: this.route,
                queryParams: {surveyId: value?.surveyId, surveyQuestionId: value?.surveyQuestionId}
            }
        ).then(() => this.validateCreateSurveyAnswerBtn());
    }

    reset(): void {
        this.router.navigate(
            ['.'],
            {
                relativeTo: this.route,
                queryParams: {surveyId: null, surveyQuestionId: null}
            }
        ).then(() => this.validateCreateSurveyAnswerBtn());
        this.filterForm.reset();
        this.surveyAnswerService.setDefaultState();
        this.surveyAnswerService.items = [];
    }

    toEdit(row?: Row): void {
        if (row && row.getData() && (row.getData() as ISurveyAnswer).id) {
            void this.router.navigate([`${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_ANSWER}/${ROUTING_CONST.DETAIL}/${(row.getData() as ISurveyAnswer).id}`])
        }

        if (!row) {
            void this.router.navigate([`${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_ANSWER}/${ROUTING_CONST.ADD}`], {
                queryParams: {surveyId: this.filterForm.controls['surveyId'].value, surveyQuestionId: this.filterForm.controls['surveyQuestionId'].value}
            })
        }
    }

    delete(row?: Row): void {
        if (confirm("Bạn có chắc chắn muốn xóa?")) {
            this.surveyAnswerService.deleteSurveyAnswer(row?.getData() as ISurveyAnswerRequest).subscribe({
                next: (): void => {
                    this.toastrService.success("Xóa thành công", 'Thành công');
                    this.search({
                        surveyId: Number(this.route.snapshot.queryParams['surveyId']),
                        surveyQuestionId: Number(this.route.snapshot.queryParams['surveyQuestionId'])
                    });
                },
                error: () => this.toastrService.danger("Có lỗi khi xóa", 'Lỗi')
            });
        }
    }

    sortPriority(): void {
        const priorityList: ISurveyAnswer[] = this.surveyAnswerService.items;
        priorityList.sort((a: ISurveyAnswer, b: ISurveyAnswer): number => {
            const p1: number = a.priority || 0;
            const p2: number = b.priority || 0;
            return p1 >= p2 ? 1 : -1;
        });
        const dialogRef: NbDialogRef<SortPriorityDialogComponent> = this.dialogService.open(SortPriorityDialogComponent, {
            context: {
                priorityList
            }
        });
        dialogRef.onClose.subscribe({
            next: (data: IBasePriorityModel[]): void => {
                if (data && data.length) {
                    this.surveyAnswerService.priority(data as ISurveyAnswer[]).subscribe({
                        next: (): void => {
                            this.toastrService.success('Sắp xếp thứ tự thành công', 'Thành công');
                            this.search({
                                surveyId: Number(this.route.snapshot.queryParams['surveyId']),
                                surveyQuestionId: Number(this.route.snapshot.queryParams['surveyQuestionId'])
                            });
                        }
                    });
                }
            }
        })
    }
}
