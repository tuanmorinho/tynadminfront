import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SurveyAnswerListComponent} from './survey-answer-list/survey-answer-list.component';
import {SurveyAnswerDetailComponent} from './survey-answer-detail/survey-answer-detail.component';
import {RouterModule, Routes} from "@angular/router";
import {DirectiveModule} from "@core/directive/directive.module";
import {FilterModule} from "@shared/filter/filter.module";
import {
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbFormFieldModule,
    NbInputModule,
    NbOptionModule,
    NbSelectModule
} from "@nebular/theme";
import {TableModule} from "@shared/table/table.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "@shared/shared.module";
import {AngularEditorModule} from "@kolkov/angular-editor";
import {ROUTING_CONST} from "@core/constants/routing-constant";
import {surveyAnswerAddGuard} from "@core/guard/survey-answer-add.guard";

const routes: Routes = [
    {
        path: ROUTING_CONST.DEFAULT,
        data: {breadcrumb: 'Đáp án khảo sát'},
        children: [
            {
                path: ROUTING_CONST.LIST,
                pathMatch: 'full',
                component: SurveyAnswerListComponent,
            },
            {
                path: ROUTING_CONST.ADD,
                pathMatch: 'full',
                canActivate: [surveyAnswerAddGuard],
                component: SurveyAnswerDetailComponent,
                data: {breadcrumb: 'Thêm mới đáp án'}
            },
            {
                path: ROUTING_CONST.DETAIL + "/:id",
                pathMatch: 'full',
                component: SurveyAnswerDetailComponent,
                data: {breadcrumb: 'Chi tiết đáp án'}
            },
            {
                path: '**', redirectTo: ROUTING_CONST.LIST
            }
        ]
    },
];

@NgModule({
    declarations: [
        SurveyAnswerListComponent,
        SurveyAnswerDetailComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        DirectiveModule,
        FilterModule,
        NbButtonModule,
        NbCardModule,
        TableModule,
        FormsModule,
        ReactiveFormsModule,
        NbFormFieldModule,
        NbInputModule,
        SharedModule,
        NbOptionModule,
        NbSelectModule,
        AngularEditorModule,
        NbCheckboxModule
    ]
})
export class SurveyAnswerModule {
}
