import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {ActivatedRoute} from "@angular/router";
import {ISurveyCategory} from "@core/model/feature";
import {FormBuilder, Validators} from "@angular/forms";
import {ROUTING_CONST, ROUTING_SURVEY_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";
import {SurveyCategoryService} from "@core/services/feature";
import {NbToastrService} from "@nebular/theme";

@Component({
  selector: 'app-survey-category-detail',
  templateUrl: './survey-category-detail.component.html',
  styleUrls: ['./survey-category-detail.component.scss']
})
export class SurveyCategoryDetailComponent extends BaseComponent implements OnInit {

  surveyCategoryId!: number | null;
  surveyCategory!: ISurveyCategory;

  private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);
  private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
  private toastrService: NbToastrService = inject<NbToastrService>(NbToastrService);
  private surveyCategoryService: SurveyCategoryService = inject<SurveyCategoryService>(SurveyCategoryService);

  ngOnInit(): void {
    this.form = this.fb.group({
      id: null,
      name: [null, Validators.compose([Validators.required, Validators.maxLength(256)])],
      description: [null, Validators.maxLength(4000)],
      imageThumbnail: [null, Validators.maxLength(4000)],
      imageFeature: [null, Validators.maxLength(4000)],
      priority: null,
    });

    this.surveyCategoryId = Number(this.route.snapshot.paramMap.get('id'));
    if (this.surveyCategoryId) {
      this.surveyCategoryService.get(this.surveyCategoryId).subscribe(data => {
        this.form.patchValue(data);
      })
    }
  }

  changeImageThumbnail(imageUrl: string): void {
    this.formControls['imageThumbnail'].setValue(imageUrl);
  }

  deleteImageThumbnail(): void {
    this.formControls['imageThumbnail'].setValue(null);
  }

  changeImageFeature(imageUrl: string): void {
    this.formControls['imageFeature'].setValue(imageUrl);
  }

  deleteImageFeature(): void {
    this.formControls['imageFeature'].setValue(null);
  }

  back(): void {
    void this.router.navigate([`/${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_CATEGORY}/${ROUTING_CONST.LIST}`]);
  }

  onSubmit(): void {
    this.form.markAllAsTouched();
    if (this.form.invalid) return;
    if (this.surveyCategoryId) {
      this.update({...this.form.value});
    } else {
      this.insert({...this.form.value});
    }
  }

  private insert(surveyCategory: ISurveyCategory): void {
    this.surveyCategoryService.insert(surveyCategory).subscribe({
      next: (): void => {
        this.toastrService.success('Thêm mới thành công', 'Thành công');
        this.back();
      },
      error: (error): void => {
        this.toastrService.danger(error.error.message, 'Lỗi');
      }
    });
  }

  private update(surveyCategory: ISurveyCategory): void {
    this.surveyCategoryService.update(surveyCategory).subscribe({
      next: (): void => {
        this.toastrService.success('Cập nhập thành công', 'Thành công');
        this.back();
      },
      error: (error): void => {
        this.toastrService.danger(error.error.message, 'Lỗi');
      }
    });
  }

}
