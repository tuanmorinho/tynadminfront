import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {FormBuilder, FormGroup} from "@angular/forms";
import {SurveyCategoryService} from "@core/services/feature";
import {IBasePriorityModel, ICellImage, IFilterModel, IPaginatorState} from "@core/model/core";
import {CellImageComponent} from "@shared/table/components/cell-image/cell-image.component";
import {ISurveyCategory} from "@core/model/feature";
import {Row} from "ng2-smart-table/lib/lib/data-set/row";
import {of} from "rxjs";
import {ROUTING_CONST, ROUTING_SURVEY_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {CellPriorityComponent} from "@shared/table/components/cell-priority/cell-priority.component";
import {SortPriorityDialogComponent} from "@shared/components/sort-priority-dialog/sort-priority-dialog.component";

@Component({
    selector: 'app-survey-category-list',
    templateUrl: './survey-category-list.component.html',
    styleUrls: ['./survey-category-list.component.scss']
})
export class SurveyCategoryListComponent extends BaseComponent implements OnInit {

    filterForm!: FormGroup;

    private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
    private toastrService: NbToastrService = inject(NbToastrService);
    private dialogService: NbDialogService = inject<NbDialogService>(NbDialogService);
    public surveyCategoryService: SurveyCategoryService = inject<SurveyCategoryService>(SurveyCategoryService);

    columns: any = {
        priority: {
            title: '',
            type: 'custom',
            width: '25px',
            onComponentInitFunction: (instance: any): void => {
                instance.change.subscribe({
                    next: (): void => { this.sortPriority(); }
                })
            },
            renderComponent: CellPriorityComponent
        },
        id: {
            title: 'ID',
            width: '20px',
        },
        name: {
            title: 'Tên loại khảo sát',
        },
        imageThumbnail: {
            title: 'Ảnh thumbnail',
            type: 'custom',
            width: '78px',
            valuePrepareFunction: (value: any): ICellImage => {
                return {image: of(this.baseUrlImage + value), options: of({width: '150', height: 'auto'})}
            },
            renderComponent: CellImageComponent
        },
        imageFeature: {
            title: 'Ảnh feature',
            type: 'custom',
            width: '78px',
            valuePrepareFunction: (value: any): ICellImage => {
                return {image: of(this.baseUrlImage + value), options: of({width: '150', height: 'auto'})}
            },
            renderComponent: CellImageComponent
        }
    }

    filterConfig: IFilterModel[] = [
        {
            type: 'input',
            label: 'Từ khóa',
            placeholder: 'Nhập tên loại khảo sát để tìm kiếm',
            inputType: 'text',
            formControlName: 'keyword',
        },
    ];

    ngOnInit(): void {
        this.filterForm = this.fb.group({
            keyword: [null],
        });
        this.find();
    }

    find(): void {
        this.surveyCategoryService.patchState({filter: {}, sorting: [{ column: 'priority', direction: 'asc' }]});
    }

    pagination(paginator: Partial<IPaginatorState>): void {
        this.surveyCategoryService.patchState({filter: paginator, sorting: [{ column: 'priority', direction: 'asc' }]});
    }

    search(value: any): void {
        this.surveyCategoryService.patchState({
            filter: value,
            sorting: [{ column: 'priority', direction: 'asc' }]
        });
    }

    reset(): void {
        this.filterForm.reset();
        this.surveyCategoryService.setDefaultState();
        this.surveyCategoryService.patchState({filter: {}, sorting: [{ column: 'priority', direction: 'asc' }]});
    }

    toEdit(row?: Row): void {
        if (row && row.getData() && (row.getData() as ISurveyCategory).id) {
            void this.router.navigate([`${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_CATEGORY}/${ROUTING_CONST.DETAIL}/${(row.getData() as ISurveyCategory).id}`])
        }

        if (!row) {
            void this.router.navigate([`${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_CATEGORY}/${ROUTING_CONST.ADD}`])
        }
    }

    delete(row?: Row): void {
        if (confirm("Bạn có chắc chắn muốn xóa?")) {
            this.surveyCategoryService.delete(row?.getData() as ISurveyCategory).subscribe({
                next: (): void => {
                    this.toastrService.success("Xóa thành công", 'Thành công');
                    this.reset();
                },
                error: () => this.toastrService.danger("Có lỗi khi xóa", 'Lỗi')
            });
        }
    }

    sortPriority(): void {
        const priorityList: ISurveyCategory[] = this.surveyCategoryService.items;
        priorityList.sort((a: ISurveyCategory, b: ISurveyCategory): number => {
            const p1: number = a.priority || 0;
            const p2: number = b.priority || 0;
            return p1 >= p2 ? 1 : -1;
        });
        const dialogRef: NbDialogRef<SortPriorityDialogComponent> = this.dialogService.open(SortPriorityDialogComponent, {
            context: {
                priorityList
            }
        });
        dialogRef.onClose.subscribe({
            next: (data: IBasePriorityModel[]): void => {
                if (data && data.length) {
                    this.surveyCategoryService.priority(data as ISurveyCategory[]).subscribe({
                        next: (): void => {
                            this.toastrService.success('Sắp xếp thứ tự thành công', 'Thành công');
                            this.reset();
                        }
                    });
                }
            }
        })
    }
}
