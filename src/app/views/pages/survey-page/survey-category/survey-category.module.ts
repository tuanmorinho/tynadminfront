import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SurveyCategoryListComponent} from './survey-category-list/survey-category-list.component';
import {SurveyCategoryDetailComponent} from './survey-category-detail/survey-category-detail.component';
import {RouterModule, Routes} from "@angular/router";
import {ROUTING_CONST} from "@core/constants/routing-constant";
import {DirectiveModule} from "@core/directive/directive.module";
import {FilterModule} from "@shared/filter/filter.module";
import {NbButtonModule, NbCardModule, NbDialogModule, NbFormFieldModule, NbInputModule} from "@nebular/theme";
import {TableModule} from "@shared/table/table.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "@shared/shared.module";

const routes: Routes = [
    {
        path: ROUTING_CONST.DEFAULT,
        data: {breadcrumb: 'Danh mục khảo sát'},
        children: [
            {
                path: ROUTING_CONST.LIST,
                pathMatch: 'full',
                component: SurveyCategoryListComponent,
            },
            {
                path: ROUTING_CONST.ADD,
                pathMatch: 'full',
                component: SurveyCategoryDetailComponent,
                data: {breadcrumb: 'Thêm mới danh mục khảo sát'}
            },
            {
                path: ROUTING_CONST.DETAIL + "/:id",
                pathMatch: 'full',
                component: SurveyCategoryDetailComponent,
                data: {breadcrumb: 'Chi tiết danh mục khảo sát'}
            },
            {
                path: '**', redirectTo: ROUTING_CONST.LIST
            }
        ]
    },
];

@NgModule({
    declarations: [
        SurveyCategoryListComponent,
        SurveyCategoryDetailComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        DirectiveModule,
        FilterModule,
        NbButtonModule,
        NbCardModule,
        TableModule,
        FormsModule,
        ReactiveFormsModule,
        NbFormFieldModule,
        NbInputModule,
        SharedModule,
        NbDialogModule.forChild()
    ]
})
export class SurveyCategoryModule {
}
