import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorPageComponent } from './error-page.component';
import {RouterModule, Routes} from "@angular/router";
import {ROUTING_CONST} from "@core/constants/routing-constant";
import {NbButtonModule, NbCardModule} from "@nebular/theme";
import {SharedModule} from "@shared/shared.module";
import {CoreModule} from "@core/module/core.module";

const routes: Routes = [
  {
    path: ROUTING_CONST.DEFAULT,
    component: ErrorPageComponent
  },
  {
    path: '**', redirectTo: ROUTING_CONST.DEFAULT
  }
]

@NgModule({
  declarations: [
    ErrorPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NbCardModule,
    NbButtonModule,
    SharedModule,
    CoreModule,
  ]
})
export class ErrorPageModule { }
