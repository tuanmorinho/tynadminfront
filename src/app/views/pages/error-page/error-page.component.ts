import {Component, inject} from '@angular/core';
import {Router} from "@angular/router";
import {ROUTING_CONST} from "@core/constants/routing-constant";

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent {

  private router: Router = inject<Router>(Router);

  backToHome(): void {
    void this.router.navigateByUrl(ROUTING_CONST.DEFAULT);
  }
}
