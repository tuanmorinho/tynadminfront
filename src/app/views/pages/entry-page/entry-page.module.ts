import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntryPageComponent } from './entry-page.component';
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
  {
    path: '',
    component: EntryPageComponent
  }
]

@NgModule({
  declarations: [
    EntryPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class EntryPageModule { }
