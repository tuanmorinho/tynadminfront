import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInPageComponent } from './sign-in-page/sign-in-page.component';
import {RouterModule, Routes} from "@angular/router";
import {ROUTING_AUTH_PAGE} from "@core/constants/routing-constant";
import {SharedModule} from "@shared/shared.module";
import {ReactiveFormsModule} from "@angular/forms";
import {
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule
} from "@nebular/theme";
import { SignOutPageComponent } from './sign-out-page/sign-out-page.component';
import {authGuard} from "@core/guard/auth.guard";
import {noAuthGuard} from "@core/guard/no-auth.guard";

const routes: Routes = [
  {
    path: ROUTING_AUTH_PAGE.SIGN_IN,
    pathMatch: 'full',
    canActivate: [noAuthGuard],
    component: SignInPageComponent
  },
  {
    path: ROUTING_AUTH_PAGE.SIGN_OUT,
    pathMatch: 'full',
    canActivate: [authGuard],
    component: SignOutPageComponent
  }
]

@NgModule({
  declarations: [
    SignInPageComponent,
    SignOutPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    SharedModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbAlertModule,
    NbFormFieldModule,
    NbIconModule
  ]
})
export class AuthPageModule { }
