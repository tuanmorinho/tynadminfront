import {Component, inject, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "@core/services/core";
import {ROUTING_AUTH_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";

@Component({
    selector: 'app-sign-out-page',
    templateUrl: './sign-out-page.component.html',
    styleUrls: ['./sign-out-page.component.scss']
})
export class SignOutPageComponent implements OnInit {

    private router: Router = inject<Router>(Router);
    private authService: AuthService = inject<AuthService>(AuthService);

    ngOnInit(): void {
        this.authService.signOut().subscribe({
            next: (): void => {
                this.router.navigateByUrl(`${ROUTING_TYN.AUTH_PAGE}/${ROUTING_AUTH_PAGE.SIGN_IN}`)
            }
        });
    }

}
