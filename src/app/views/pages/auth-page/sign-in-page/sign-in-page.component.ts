import {Component, inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BaseComponent} from "@core/module/base/base.component";
import {last} from "rxjs";
import {AuthService} from "@core/services/core/auth.service";
import {ActivatedRoute} from "@angular/router";
import {CustomFormValidators} from "@core/validators/form-validator";
import {NbComponentStatus} from "@nebular/theme/components/component-status";

@Component({
  selector: 'app-sign-in-page',
  templateUrl: './sign-in-page.component.html',
  styleUrls: ['./sign-in-page.component.scss']
})
export class SignInPageComponent extends BaseComponent implements OnInit {
  signInForm!: FormGroup;
  showAlert: boolean = false;

  alert: { status: NbComponentStatus; message: string } = {
    status: 'success',
    message: '',
  };

  private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
  private authService: AuthService = inject<AuthService>(AuthService);
  private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);

  ngOnInit(): void {
    this.signInForm = this.fb.group({
      username: ['', [Validators.required, CustomFormValidators.emailValidator]],
      password: ['', [Validators.required, CustomFormValidators.passwordValidator]],
    });
  }

  get v() {
    return this.signInForm.value;
  }

  get f() {
    return this.signInForm.controls;
  }

  signIn(): void {
    this.signInForm.markAllAsTouched();
    if (this.signInForm.invalid) return;
    this.signInForm.disable();
    this.showAlert = false;
    this.authService.signIn(this.v)
      .pipe(last())
      .subscribe({
        next: (data: { token: string }): void => {
          if (data && data.token) {
            let redirectURL =
              this.route.snapshot.queryParamMap.get(
                'redirectUrl'
              ) || '/';
            if (redirectURL === '/error') {
              redirectURL = '/'
            }
            this.router.navigateByUrl(redirectURL);
          }
        },
        error: (err): void => {
          this.signInForm.enable();
          this.signInForm.reset();
          this.alert = {
            status: 'danger',
            message: err?.error?.message
          };
          this.showAlert = true;
        }
      })
  }
}
