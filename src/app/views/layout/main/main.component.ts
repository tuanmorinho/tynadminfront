import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {NbSidebarState} from "@nebular/theme";
import {distinctUntilChanged, filter} from "rxjs";
import {ActivatedRoute, Data, Event, NavigationEnd, Scroll} from "@angular/router";
import {TLayout} from "@core/model/core/layout.model";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent extends BaseComponent implements OnInit {

  sideBarState: NbSidebarState = 'expanded';
  layout!: TLayout;

  private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);

  ngOnInit(): void {
    this.watchLayoutChange();
  }

  setSideBarState(state: NbSidebarState): void {
    this.sideBarState = state;
  }

  private watchLayoutChange(): void {
    this.router.events.pipe(
      filter((event: Event): boolean => event instanceof NavigationEnd || event instanceof Scroll),
      distinctUntilChanged(),
    ).subscribe({
      next: (event: Event): void => {
        const baseRouteData: Data | undefined = this.route.firstChild?.snapshot.data;
        if (baseRouteData && baseRouteData['layout']) {
          this.layout = baseRouteData['layout'];
        }
      }
    })
  }
}
