import {ChangeDetectionStrategy, Component, EventEmitter, inject, Input, OnInit, Output} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {NbSidebarState} from "@nebular/theme";
import {AuthService} from "@core/services/core/auth.service";
import {Observable} from "rxjs";
import {IAuthUser, IMenuHeaderModel} from "@core/model/core";
import {MENU_HEADER} from "@core/constants/menu-constant";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent extends BaseComponent implements OnInit {
  @Input() set sideBarState(value: NbSidebarState) {
    if (value) this.stateSideBar = value;
  };

  @Output() toggleSideBar: EventEmitter<NbSidebarState> = new EventEmitter<NbSidebarState>();

  protected stateSideBar: NbSidebarState = 'expanded';
  protected menuItems: IMenuHeaderModel[] = MENU_HEADER;
  protected currentUser$!: Observable<IAuthUser | null>;

  private authService: AuthService = inject<AuthService>(AuthService);

  ngOnInit(): void {
    this.currentUser$ = this.authService.currentUserStore$;
  }

  toggleStateSideBar(state: NbSidebarState): void {
    this.stateSideBar = state;
    this.toggleSideBar.emit(state);
  }
}
