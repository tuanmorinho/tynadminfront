import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadingComponent implements OnInit, OnDestroy {

  ngOnDestroy(): void {
    document.documentElement.style.overflow = 'visible';
  }

  ngOnInit(): void {
    document.documentElement.style.overflow = 'hidden';
  }
}
