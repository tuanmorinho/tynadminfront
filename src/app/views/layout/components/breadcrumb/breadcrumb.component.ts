import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {ActivatedRoute, Event, NavigationEnd, Scroll} from "@angular/router";
import {distinctUntilChanged, filter} from "rxjs";
import {IBreadCrumb} from "@core/model/core";

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent extends BaseComponent implements OnInit {

  breadcrumbs: IBreadCrumb[] = [];

  private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);

  constructor() {
    super();
    this.breadcrumbs = this.buildBreadCrumb(this.route.root);
  }

  ngOnInit(): void {
    this.watchBreadcrumbChange();
  }

  private watchBreadcrumbChange(): void {
    this.router.events.pipe(
      filter((event: Event): boolean => event instanceof NavigationEnd || event instanceof Scroll),
      distinctUntilChanged(),
    ).subscribe({
      next: (): void => {
        this.breadcrumbs = this.buildBreadCrumb(this.route.root);
      }
    })
  }

  private buildBreadCrumb(route: ActivatedRoute, url: string = '', breadcrumbs: IBreadCrumb[] = []): IBreadCrumb[] {
    let label: string = route.routeConfig && route.routeConfig.data ? route.routeConfig.data['breadcrumb'] : '';
    let path: string | undefined = route.routeConfig && route.routeConfig.data ? route.routeConfig.path : '';

    // If the route is dynamic route such as ':id', remove it
    const lastRoutePart: string | undefined = path && path.split('/').pop();
    const isDynamicRoute: boolean = Boolean(lastRoutePart && lastRoutePart.startsWith(':'));
    if (isDynamicRoute && lastRoutePart && path && route.snapshot) {
      const paramName: string = lastRoutePart.split(':')[1];
      path = path.replace(lastRoutePart, route.snapshot.params[paramName]);
    }
    // In the routeConfig the complete path is not available,
    // so we rebuild it each time
    const nextUrl = path ? `${url}/${path}` : url;
    const breadcrumb: IBreadCrumb = {
      label: label,
      url: nextUrl,
    };
    // Only adding route with non-empty label
    const newBreadcrumbs = breadcrumb.label ? [...breadcrumbs, breadcrumb] : [...breadcrumbs];
    if (route.firstChild) {
      // If we are not on our current path yet,
      // there will be more children to look after, to build our breadcumb
      return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
    }
    return newBreadcrumbs;
  }
}
