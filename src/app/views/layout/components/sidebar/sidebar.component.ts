import { Component } from '@angular/core';
import {NbMenuItem} from "@nebular/theme";
import {MENU_SIDEBAR} from "@core/constants/menu-constant";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  navigationItems: NbMenuItem[] = MENU_SIDEBAR;
}
