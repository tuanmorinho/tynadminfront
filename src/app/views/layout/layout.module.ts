import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainComponent} from './main/main.component';
import {RouterModule} from "@angular/router";
import {
    NbActionsModule,
    NbCardModule,
    NbContextMenuModule,
    NbIconModule,
    NbLayoutModule,
    NbMenuModule,
    NbPopoverModule,
    NbSidebarModule,
    NbSpinnerModule
} from "@nebular/theme";
import {HeaderComponent} from './components/header/header.component';
import {BreadcrumbComponent} from './components/breadcrumb/breadcrumb.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {LoadingComponent} from './components/loading/loading.component';
import {DirectiveModule} from "@core/directive/directive.module";


@NgModule({
    declarations: [
        MainComponent,
        HeaderComponent,
        BreadcrumbComponent,
        SidebarComponent,
        LoadingComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        NbLayoutModule,
        NbSidebarModule,
        NbIconModule,
        NbActionsModule,
        NbCardModule,
        NbMenuModule,
        NbSpinnerModule,
        NbContextMenuModule,
        NbPopoverModule,
        DirectiveModule
    ],
    exports: [
        MainComponent,
    ]
})
export class LayoutModule {
}
