import {IMenuHeaderModel} from "@core/model/core";
import {NbMenuItem} from "@nebular/theme";
import {ROUTING_AUTH_PAGE, ROUTING_SURVEY_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";

export const MENU_HEADER: IMenuHeaderModel[] = [
    // {
    //     label: 'Tài khoản',
    //     icon: 'person-outline',
    // },
    {
        label: 'Đăng xuất',
        icon: 'log-out',
        link: `/${ROUTING_TYN.AUTH_PAGE}/${ROUTING_AUTH_PAGE.SIGN_OUT}`,
        role: ['USER', 'ADMIN', 'ROOT', 'ACCOUNT', 'SURVEY', 'MEDIA', 'FILE', 'DEFAULT']
    }
]

export const MENU_SIDEBAR: NbMenuItem[] = [
    // {
    //     title: 'Trang chủ',
    //     icon: 'loader-outline',
    //     link: ''
    // },
    // {
    //   title: 'Hệ thống',
    //   icon: 'settings-outline',
    //   expanded: true,
    //   children: [
    //     {
    //       title: 'Phân quyền',
    //       icon: 'lock-outline'
    //     },
    //     {
    //       title: 'Người dùng',
    //       icon: 'person-outline'
    //     },
    //   ],
    // },
    {
        title: 'Quản lý bộ khảo sát',
        icon: 'layers-outline',
        expanded: true,
        children: [
            {
                title: 'Danh mục khảo sát',
                icon: 'inbox-outline',
                link: `/${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_CATEGORY}`,
                pathMatch: 'prefix'
            },
            {
                title: 'Khảo sát',
                icon: 'book-open-outline',
                link: `/${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY}`,
                pathMatch: 'prefix'
            },
            {
                title: 'Câu hỏi khảo sát',
                icon: 'question-mark-circle-outline',
                link: `/${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_QUESTION}`,
                pathMatch: 'prefix'
            },
            {
                title: 'Đáp án khảo sát',
                icon: 'checkmark-circle-outline',
                link: `/${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_ANSWER}`,
                pathMatch: 'prefix'
            },
            {
                title: 'Cấu hình khảo sát',
                icon: 'shuffle-2-outline',
                link: `/${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_QUESTION_STEP}`,
                pathMatch: 'prefix'
            }
        ],
    },
];
