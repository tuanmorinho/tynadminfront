export const ROUTING_CONST = {
  DEFAULT: '',
  LIST: 'list',
  ADD: 'add',
  DETAIL: 'detail'
}

export const ROUTING_TYN = {
  DEFAULT: '',
  AUTH_PAGE: 'auth',
  SURVEY_PAGE: 'survey-management',
}

export const ROUTING_AUTH_PAGE = {
  SIGN_IN: 'sign-in',
  SIGN_OUT: 'sign-out'
}

export const ROUTING_SURVEY_PAGE = {
  SURVEY_CATEGORY: 'survey-category',
  SURVEY: 'survey',
  SURVEY_QUESTION: 'survey-question',
  SURVEY_ANSWER: 'survey-answer',
  SURVEY_QUESTION_STEP: 'survey-question-step',
}
