import {Injectable, OnDestroy} from '@angular/core';
import {TableService} from "@core/services/core";
import {ISurveyQuestion, ISurveyQuestionRequest} from "@core/model/feature";
import {environment} from "../../../../enviroments";
import {ITableState} from "@core/model/core/table-state.model";
import {map, Observable, Subscription} from "rxjs";
import {Pagination} from "@core/model/core";

@Injectable({
    providedIn: 'root'
})
export class SurveyQuestionService extends TableService<ISurveyQuestion> implements OnDestroy {

    override API_URL: string = environment.apiUrl.base + '/survey-question';

    override find(tableState: ITableState): Observable<Pagination<ISurveyQuestion>> {
        const data: any = this.nomarlizedTableState(tableState);
        return this.http.post(this.API_URL + '/findBySearch', data).pipe(
            map((res: any): Pagination<ISurveyQuestion> => ({
                content: res?.content || [],
                pageable: res?.pageable || {},
                totalElements: res?.totalElements
            }))
        );
    }

    detailSurveyQuestion(id: number): Observable<ISurveyQuestion> {
        return this.http.get<ISurveyQuestion>(this.API_URL + '/detailSurveyQuestion/' + id);
    }

    createSurveyQuestion(surveyQuestionRequest: ISurveyQuestionRequest): Observable<ISurveyQuestion> {
        return this.http.post<ISurveyQuestion>(this.API_URL + '/createSurveyQuestion', surveyQuestionRequest);
    }

    updateSurveyQuestion(surveyQuestionRequest: ISurveyQuestionRequest): Observable<ISurveyQuestion> {
        return this.http.post<ISurveyQuestion>(this.API_URL + '/updateSurveyQuestion/' + surveyQuestionRequest.id, surveyQuestionRequest);
    }

    deleteSurveyQuestion(surveyQuestionRequest: ISurveyQuestionRequest): Observable<ISurveyQuestion> {
        return this.http.post<ISurveyQuestion>(this.API_URL + '/deleteSurveyQuestion/' + surveyQuestionRequest.id, surveyQuestionRequest);
    }

    validateCreateSurveyQuestion(surveyQuestionRequest: ISurveyQuestionRequest): Observable<boolean> {
        return this.http.post<boolean>(this.API_URL + '/validateCreateSurveyQuestion', surveyQuestionRequest);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sb: Subscription) => sb.unsubscribe());
        this.items = [];
    }
}
