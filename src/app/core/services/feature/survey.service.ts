import {Injectable, OnDestroy} from '@angular/core';
import {TableService} from "@core/services/core";
import {ISurvey, ISurveyRequest} from "@core/model/feature";
import {environment} from "../../../../enviroments";
import {ITableState} from "@core/model/core/table-state.model";
import {map, Observable, Subscription} from "rxjs";
import {IStatusInfo, Pagination} from "@core/model/core";

@Injectable({
  providedIn: 'root'
})
export class SurveyService extends TableService<ISurvey> implements OnDestroy {

  override API_URL: string = environment.apiUrl.base + '/survey';

  override find(tableState: ITableState): Observable<Pagination<ISurvey>> {
    const data: any = this.nomarlizedTableState(tableState);
    return this.http.post(this.API_URL + '/findBySearch', data).pipe(
      map((res: any): Pagination<ISurvey> => ({
        content: res?.content || [],
        pageable: res?.pageable || {},
        totalElements: res?.totalElements
      }))
    );
  }

  get(id: number): Observable<ISurvey> {
    return this.http.get<ISurvey>(this.API_URL + '/get/' + id);
  }

  detailSurvey(id: number): Observable<ISurvey> {
    return this.http.get<ISurvey>(this.API_URL + '/detailSurvey/' + id);
  }

  insert(survey: ISurvey): Observable<ISurvey> {
    return this.http.post<ISurvey>(this.API_URL + '/insert', survey);
  }

  createSurvey(surveyRequest: ISurveyRequest): Observable<ISurvey> {
    return this.http.post<ISurvey>(this.API_URL + '/createSurvey', surveyRequest);
  }

  update(survey: ISurvey): Observable<ISurvey> {
    return this.http.post<ISurvey>(this.API_URL + '/update/' + survey.id, survey);
  }

  updateSurvey(surveyRequest: ISurveyRequest): Observable<ISurvey> {
    return this.http.post<ISurvey>(this.API_URL + '/updateSurvey/' + surveyRequest.id, surveyRequest);
  }

  delete(survey: ISurvey): Observable<ISurvey> {
    return this.http.post<ISurvey>(this.API_URL + '/delete/' + survey.id, survey);
  }

  trash(survey: ISurvey): Observable<ISurvey> {
    return this.http.post<ISurvey>(this.API_URL + '/trash/' + survey.id, survey);
  }

  restore(survey: ISurvey): Observable<ISurvey> {
    return this.http.post<ISurvey>(this.API_URL + '/restore/' + survey.id, survey);
  }

  statusInfo(): Observable<IStatusInfo> {
    return this.http.get<IStatusInfo>(this.API_URL + '/statusInfo');
  }

  updateStatus(survey: ISurvey): Observable<ISurvey> {
    return this.http.post<ISurvey>(this.API_URL + '/updateStatus/' + survey.id, survey);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sb: Subscription) => sb.unsubscribe());
    this.items = [];
  }
}
