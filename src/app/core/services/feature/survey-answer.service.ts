import {Injectable, OnDestroy} from '@angular/core';
import {TableService} from "@core/services/core";
import {ISurveyAnswer, ISurveyAnswerRequest} from "@core/model/feature";
import {environment} from "../../../../enviroments";
import {map, Observable, Subscription} from "rxjs";
import {ITableState} from "@core/model/core/table-state.model";
import {Pagination} from "@core/model/core";

@Injectable({
    providedIn: 'root'
})
export class SurveyAnswerService extends TableService<ISurveyAnswer> implements OnDestroy {

    override API_URL: string = environment.apiUrl.base + '/survey-answer';

    override find(tableState: ITableState): Observable<Pagination<ISurveyAnswer>> {
        const data: any = this.nomarlizedTableState(tableState);
        return this.http.post(this.API_URL + '/findBySearch', data).pipe(
            map((res: any): Pagination<ISurveyAnswer> => ({
                content: res?.content || [],
                pageable: res?.pageable || {},
                totalElements: res?.totalElements
            }))
        );
    }

    findAllSurveyAnswer(surveyQuestionId: number): Observable<ISurveyAnswer[]> {
        return this.http.get<ISurveyAnswer[]>(this.API_URL + '/findAllSurveyAnswer/' + surveyQuestionId);
    }

    detailSurveyAnswer(id: number): Observable<ISurveyAnswer> {
        return this.http.get<ISurveyAnswer>(this.API_URL + '/detailSurveyAnswer/' + id);
    }

    createSurveyAnswer(surveyAnswerRequest: ISurveyAnswerRequest): Observable<ISurveyAnswer> {
        return this.http.post<ISurveyAnswer>(this.API_URL + '/createSurveyAnswer', surveyAnswerRequest);
    }

    updateSurveyAnswer(surveyAnswerRequest: ISurveyAnswerRequest): Observable<ISurveyAnswer> {
        return this.http.post<ISurveyAnswer>(this.API_URL + '/updateSurveyAnswer/' + surveyAnswerRequest.id, surveyAnswerRequest);
    }

    deleteSurveyAnswer(surveyAnswerRequest: ISurveyAnswerRequest): Observable<ISurveyAnswer> {
        return this.http.post<ISurveyAnswer>(this.API_URL + '/deleteSurveyAnswer/' + surveyAnswerRequest.id, surveyAnswerRequest);
    }

    priority(surveyAnswerList: ISurveyAnswer[]): Observable<ISurveyAnswer[]> {
        return this.http.put<ISurveyAnswer[]>(this.API_URL + '/priority', surveyAnswerList);
    }

    validateCreateSurveyAnswer(surveyAnswerRequest: ISurveyAnswerRequest): Observable<boolean> {
        return this.http.post<boolean>(this.API_URL + '/validateCreateSurveyAnswer', surveyAnswerRequest);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sb: Subscription) => sb.unsubscribe());
        this.items = [];
    }
}
