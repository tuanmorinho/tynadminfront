import {inject, Injectable, OnDestroy} from '@angular/core';
import {environment} from "../../../../enviroments";
import {map, Observable, Subscription} from "rxjs";
import {IMovie} from "@core/model/feature";
import {Pagination} from "@core/model/core";
import {TableService} from "@core/services/core";
import {ITableState} from "@core/model/core/table-state.model";

@Injectable({
  providedIn: 'root'
})
export class MovieService extends TableService<IMovie> implements OnDestroy {

  override API_URL: string = environment.apiUrl.base + '/v1/adminMovie';

  override find(tableState: ITableState): Observable<Pagination<IMovie>> {
    const data: any = this.nomarlizedTableState(tableState);
    return this.http.post(this.API_URL + '/findMovie', data).pipe(
      map((res: any): Pagination<IMovie> => ({
        content: res?.content || [],
        pageable: res?.pageable || {},
        totalElements: res?.totalElements
      }))
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sb: Subscription) => sb.unsubscribe());
    this.items = [];
  }
}
