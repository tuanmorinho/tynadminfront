import {Injectable, OnDestroy} from '@angular/core';
import {TableService} from "@core/services/core";
import {ISurveyCategory} from "@core/model/feature";
import {environment} from "../../../../enviroments";
import {ITableState} from "@core/model/core/table-state.model";
import {map, Observable, Subscription} from "rxjs";
import {Pagination} from "@core/model/core";

@Injectable({
  providedIn: 'root'
})
export class SurveyCategoryService extends TableService<ISurveyCategory> implements OnDestroy {

  override API_URL: string = environment.apiUrl.base + '/survey-category';

  override find(tableState: ITableState): Observable<Pagination<ISurveyCategory>> {
    const data: any = this.nomarlizedTableState(tableState);
    return this.http.post(this.API_URL + '/findBySearch', data).pipe(
      map((res: any): Pagination<ISurveyCategory> => ({
        content: res?.content || [],
        pageable: res?.pageable || {},
        totalElements: res?.totalElements
      }))
    );
  }

  get(id: number): Observable<ISurveyCategory> {
    return this.http.get<ISurveyCategory>(this.API_URL + '/get/' + id);
  }

  insert(surveyCategory: ISurveyCategory): Observable<ISurveyCategory> {
    return this.http.post<ISurveyCategory>(this.API_URL + '/insert', surveyCategory);
  }

  update(surveyCategory: ISurveyCategory): Observable<ISurveyCategory> {
    return this.http.post<ISurveyCategory>(this.API_URL + '/update/' + surveyCategory.id, surveyCategory);
  }

  delete(surveyCategory: ISurveyCategory): Observable<ISurveyCategory> {
    return this.http.post<ISurveyCategory>(this.API_URL + '/delete/' + surveyCategory.id, surveyCategory);
  }

  priority(surveyCategoryList: ISurveyCategory[]): Observable<ISurveyCategory[]>  {
    return this.http.put<ISurveyCategory[]>(this.API_URL + '/priority', surveyCategoryList);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sb: Subscription) => sb.unsubscribe());
    this.items = [];
  }
}
