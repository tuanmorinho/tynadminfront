import {inject, Injectable} from '@angular/core';
import {environment} from "../../../../enviroments";
import {HttpClient} from "@angular/common/http";
import {ISurveyQuestionStep} from "@core/model/feature";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class SurveyQuestionStepService {

    private API_URL: string = environment.apiUrl.base + '/survey-question-step';

    private http: HttpClient = inject<HttpClient>(HttpClient);

    doMappingStep(stepMapping: ISurveyQuestionStep[]): Observable<any> {
        return this.http.post<any>(this.API_URL + '/mapping-step', stepMapping);
    }

    detailStepMapping(surveyId: number): Observable<ISurveyQuestionStep[]> {
        return this.http.get<ISurveyQuestionStep[]>(this.API_URL + '/detail/' + surveyId);
    }
}
