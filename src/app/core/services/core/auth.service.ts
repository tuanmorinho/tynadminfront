import {inject, Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of, shareReplay, switchMap} from "rxjs";
import {IAuthUser} from "@core/model/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../enviroments";
import jwt_decode from "jwt-decode";
import {AuthUtils} from "@core/utils";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUser$: BehaviorSubject<IAuthUser | null> = new BehaviorSubject<IAuthUser | null>(null);
  public currentUserStore$: Observable<IAuthUser | null> = this.currentUser$.pipe(shareReplay<IAuthUser | null>({bufferSize: 1, refCount: true}));

  private url: string = environment.apiUrl.base + '/auth';

  private http: HttpClient = inject<HttpClient>(HttpClient);

  get currentUserSnapshot(): IAuthUser | null {
    return this.currentUser$.value;
  }

  get tokenLocalStorage(): string | null  {
    return localStorage.getItem('tk');
  };

  signIn(credentials: { username: string; password: string }): Observable<{ token: string }> {
    return this.http.post<{ token: string }>(`${this.url}/login`, credentials).pipe(
      switchMap((res: { token: string }) => {
        const decoded: IAuthUser = jwt_decode(res.token);
        this.setAuthFromLocalStorage(res);
        decoded.token = res.token;
        this.currentUser$.next(decoded);
        return of(res);
      })
    );
  }

  signOut(): Observable<boolean> {
    localStorage.removeItem('tk');
    localStorage.removeItem('us');
    this.currentUser$.next(null);
    return of(true);
  }

  getUserByToken(): IAuthUser | boolean | undefined {
    if (!this.tokenLocalStorage) {
      return false;
    }

    const userFromToken: IAuthUser = jwt_decode(this.tokenLocalStorage);
    if (!userFromToken) {
      return undefined;
    }

    this.currentUser$.next(userFromToken as IAuthUser);
    return userFromToken;
  }

  checkAuth(): Observable<boolean> {
    if (!this.tokenLocalStorage) {
      return of(false);
    }

    if (AuthUtils.isTokenExpired(this.tokenLocalStorage)) {
      return of(false);
    }

    if (this.getUserByToken()) {
      return of(true);
    }

    return of(false);
  }

  private setAuthFromLocalStorage(auth: { token: string }): void {
    const decoded: IAuthUser = jwt_decode(auth.token);
    localStorage.setItem('tk', auth.token);
    localStorage.setItem('us', JSON.stringify(decoded));
  }
}
