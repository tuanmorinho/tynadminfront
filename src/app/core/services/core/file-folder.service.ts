import {inject, Injectable} from '@angular/core';
import {environment} from "../../../../enviroments";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IFileFolder, IFileFolderListResponse, IFileFolderRequest} from "@core/model/core";

@Injectable({
  providedIn: 'root'
})
export class FileFolderService {
  private url: string = environment.apiUrl.base + '/folder';

  private http: HttpClient = inject<HttpClient>(HttpClient);

  viewFolder(fileFolder: IFileFolder | {}): Observable<IFileFolderListResponse> {
    return this.http.post<IFileFolderListResponse>(`${this.url}/view`, fileFolder);
  }

  doCreateFolder(fileFolderRequest: IFileFolderRequest): Observable<IFileFolder> {
    return this.http.post<IFileFolder>(`${this.url}/create`, fileFolderRequest);
  }

  doDeleteFolder(fileFolder: IFileFolder): Observable<IFileFolder> {
    return this.http.post<IFileFolder>(`${this.url}/delete/${fileFolder.id}`, fileFolder);
  }
}
