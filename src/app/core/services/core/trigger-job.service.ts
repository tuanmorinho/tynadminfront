import {inject, Injectable} from '@angular/core';
import {environment} from "../../../../enviroments";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TriggerJobService {

  private url: string = environment.apiUrl.base + '/job';

  private http: HttpClient = inject<HttpClient>(HttpClient);

  triggerCountSurveyJob(): Observable<void> {
    return this.http.post<void>(`${this.url}/trigg`, null);
  }
}
