import { Injectable } from '@angular/core';
import {BehaviorSubject, shareReplay} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoadingStoreService {

  private _isLoading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isLoading$ = this._isLoading$.pipe(shareReplay({refCount: true, bufferSize: 1}));

  set isLoading(val: boolean) {
    this._isLoading$.next(val);
  }
}
