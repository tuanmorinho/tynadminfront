import {BehaviorSubject, catchError, Observable, of, shareReplay, Subscription, tap} from "rxjs";
import {Pagination, PaginatorState, SortState} from "@core/model/core";
import {DEFAULT_STATE, ITableState} from "@core/model/core/table-state.model";
import {environment} from "../../../../enviroments";
import {HttpClient} from "@angular/common/http";
import {inject} from "@angular/core";
import {LoadingStoreService} from "@core/services/core/loading-store.service";

export abstract class TableService<T> {
  private _items$: BehaviorSubject<T[]> = new BehaviorSubject<T[]>([]);
  public items$: Observable<T[]> = this._items$.pipe(shareReplay({refCount: true, bufferSize: 1}));
  private _tableState$: BehaviorSubject<ITableState> = new BehaviorSubject<ITableState>(DEFAULT_STATE);
  public tableState$: Observable<ITableState> = this._tableState$.pipe(shareReplay({refCount: true, bufferSize: 1}));
  private _subscriptions: Subscription[] = [];

  // Override when extends
  protected API_URL: string = environment.apiUrl.base;

  protected http: HttpClient = inject<HttpClient>(HttpClient);
  public loadingStore: LoadingStoreService = inject<LoadingStoreService>(LoadingStoreService);

  set items(val: T[]) {
    this._items$.next(val);
  }

  get items() {
    return this._items$.value;
  }

  setTableState(val: ITableState & Partial<ITableState>): void {
    this._tableState$.next(val);
  }

  get subscriptions() {
    return this._subscriptions;
  }

  protected find(tableState: ITableState): Observable<Pagination<T>> {
    return this.http.post<Pagination<T>>(this.API_URL, tableState).pipe(
      catchError(() => {
        return of({ content: [], pageable: DEFAULT_STATE.paginator, totalElements: 0 });
      })
    );
  }

  public fetch(disableLoading?: boolean): void {
    if (!disableLoading) this.loadingStore.isLoading = true;
    const request: Subscription = this.find(this._tableState$.value).pipe(
      tap((res: Pagination<T>): void => {
        this._items$.next(res.content);
        this.patchStateWithoutFetch({
          paginator: this._tableState$.value.paginator.recalculatePaginator(
            res.totalElements
          ),
        });
      }),
      catchError(() => {
        return of({ content: [], pageable: DEFAULT_STATE.paginator });
      })
    ).subscribe({
      next: (): void => {
        if (!disableLoading) this.loadingStore.isLoading = false;
      }
    });
    this._subscriptions.push(request);
  }

  public patchState(patch: Partial<ITableState>, disableLoading?: boolean): void {
    this.patchStateWithoutFetch(patch);
    this.fetch(disableLoading);
  }

  public patchStateWithoutFetch(patch: Partial<ITableState>): void {
    const newState: ITableState & Partial<ITableState> = Object.assign(this._tableState$.value, patch);
    this._tableState$.next(newState);
  }

  public setDefaultState(): void {
    this.patchStateWithoutFetch({ filter: {} });
    this.patchStateWithoutFetch({ sorting: [new SortState()] });
    this.patchStateWithoutFetch({
      paginator: new PaginatorState()
    });
    this._tableState$.next(DEFAULT_STATE);
  }

  public nomarlizedTableState(tableState: ITableState): any {
    let params: any = {};
    params['page'] = tableState.paginator.page;
    params['take'] = tableState.paginator.take;

    if (tableState.sorting && tableState.sorting.length > 0) {
      let sort: string[] = [];
      for (let sortState of tableState.sorting) {
        if (Object.keys(sortState).length !== 0 && sortState.column != 'No') {
          sort.push(sortState.direction === 'desc' ? '-' + sortState.column : sortState.column);
        }
      }
      params['sort'] = sort.toString();
    }

    if (Object.keys(tableState.filter).length !== 0) {
      for (let key in tableState.filter) {
        params[key] = typeof tableState.filter[key] === "string" ? tableState.filter[key].trim() : tableState.filter[key];
      }
    }
    return params;
  }
}
