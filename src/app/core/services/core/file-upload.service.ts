import {inject, Injectable} from '@angular/core';
import {environment} from "../../../../enviroments";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IFileStorage} from "@core/model/core";

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {
  private url: string = environment.apiUrl.base + '/file';

  private http: HttpClient = inject<HttpClient>(HttpClient);

  uploadFile(formData: FormData): Observable<IFileStorage> {
    return this.http.post<IFileStorage>(`${this.url}/upload`, formData);
  }

  doDeleteFile(fileStorage: IFileStorage): Observable<IFileStorage> {
    return this.http.post<IFileStorage>(this.url + '/delete/' + fileStorage.id, fileStorage);
  }
}
