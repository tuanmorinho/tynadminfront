import {inject, Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import {catchError, Observable, throwError} from 'rxjs';
import {AuthService} from "@core/services/core/auth.service";
import {AuthUtils} from "@core/utils";
import {Router} from "@angular/router";
import {ROUTING_AUTH_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  private authService: AuthService = inject<AuthService>(AuthService);
  private router: Router = inject<Router>(Router);

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let newRequest: HttpRequest<any> = request.clone();
    const token: string | null = localStorage.getItem('tk');

    if (token && !AuthUtils.isTokenExpired(token)) {
      newRequest = request.clone({
        headers: request.headers
          .set('Authorization', 'Bearer ' + token)
      });
    }

    return next.handle(newRequest).pipe(
      catchError(err => {
        if (err instanceof HttpErrorResponse && err.status === 401) {
          this.authService.signOut();
          this.router.navigateByUrl(`${ROUTING_TYN.AUTH_PAGE}/${ROUTING_AUTH_PAGE.SIGN_OUT}`);
        }
        return throwError(err);
      })
    );
  }
}
