import {APP_INITIALIZER, Provider} from "@angular/core";
import {authInitializer, triggerCountSurveyJobInitializer} from "@core/DI/app-init";
import {AuthService} from "@core/services/core/auth.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {AuthInterceptor} from "@core/interceptor/auth.interceptor";
import {TriggerJobService} from "@core/services/core";

export const AppProviderFactory: Provider[] = [
  {
    provide: APP_INITIALIZER,
    useFactory: authInitializer,
    deps: [AuthService],
    multi: true
  },
  // {
  //   provide: APP_INITIALIZER,
  //   useFactory: triggerCountSurveyJobInitializer,
  //   deps: [TriggerJobService],
  //   multi: true
  // },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }
]
