import {AuthService} from "@core/services/core/auth.service";
import {TriggerJobService} from "@core/services/core";

export const authInitializer = (authService: AuthService) => {
  return () => authService.getUserByToken();
};

export const triggerCountSurveyJobInitializer = (triggerJobService: TriggerJobService) => {
  return () => triggerJobService.triggerCountSurveyJob();
}
