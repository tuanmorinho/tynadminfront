import {Observable} from "rxjs";
import {Pipe} from "@angular/core";

export interface IFilterModel {
  label: string;
  placeholder: string;
  formControlName: string;
  type: TypeFilterFormControl;
  isRequired?: boolean;

  // input
  inputType?: string;

  // select
  options?: any[];
  bindLabel?: string;
  bindValue?: string;

  // select Observable
  itemObservable?: Observable<any>;
  propertyName?: string;

  // select Observable filter pipe
  pipe?: Pipe;
  argsPipe?: string;

  // select choose
  component?: any;
  controlForDisplay?: string;
}

export type TypeFilterFormControl = 'input' | 'select' | 'select_observable' | 'input_date' | 'input_choose';
