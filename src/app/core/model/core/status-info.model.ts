import {IBaseModel} from "@core/model/core/base.model";

export interface IStatusCategoryBase extends IBaseModel {
  id: number;
  type: string;
  name: string;
  label: string;
  value: number;
  description: string;
}

export interface IStatusCategory extends IStatusCategoryBase {
}

export interface IStatusCategoryInfo extends IStatusCategory {
  count: number;
}

export interface IStatusInfo {
  statusCount: IStatusCategoryInfo[];
  trashCount: IStatusCategoryInfo[];
}
