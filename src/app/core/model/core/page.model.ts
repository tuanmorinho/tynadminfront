export interface Pageable {
  page: number;
  take: number;
}

export interface Pagination<T> {
  content: Array<T>;
  pageable: Pageable;
  totalElements: number;
}

export const PageSizes: number[] = [10, 20, 50, 100];

export interface IPaginatorState {
  page: number;
  take: number;
  total: number;
  recalculatePaginator(total: number): IPaginatorState;
}

export class PaginatorState implements IPaginatorState {
  page: number = 1;
  take: number = PageSizes[0];
  total: number = 0;

  recalculatePaginator(totalElements: number): IPaginatorState {
    this.total = totalElements;
    return this;
  }
}

export class SortState {
  column: string = 'id'; // id by default
  direction: 'asc' | 'desc' = 'asc'; // asc by default;
}
