export interface IMenuHeaderModel {
    label: string;
    icon: string;
    link: string;
    role: string[];
}
