import {PaginatorState, SortState} from "@core/model/core/page.model";

export interface ITableState {
  filter: any;
  paginator: PaginatorState;
  sorting: SortState[];
}

export const DEFAULT_STATE: ITableState = {
  filter: {},
  paginator: new PaginatorState(),
  sorting: [new SortState()]
};
