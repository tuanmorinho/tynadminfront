export interface IBasePriorityModel {
    id?: number;
    name?: string;
    priority?: number;
}
