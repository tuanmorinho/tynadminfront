import {IBaseModel} from "@core/model/core/base.model";
import {IFileStorage} from "@core/model/core/file-storage.model";

export interface IFileFolderBase extends IBaseModel {
  name?: string;
}

export interface IFileFolder extends IFileFolderBase {
}

export interface IFileFolderListResponse {
  folderList?: IFileFolder[];
  fileList?: IFileStorage[];
}

export interface IFileForChoose extends IFileStorage {
  checked: boolean;
  disable: boolean
}

export interface IFileFolderRequest {
  name: string;
  fileFolderParentId: number | null;
}
