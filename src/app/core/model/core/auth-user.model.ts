import {IAccountModel} from "@core/model/feature";

export interface IAuthUser {
  roles: string[],
  sub: string;
  token: string;
  account: IAccountModel;
}
