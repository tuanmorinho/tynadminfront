import {Observable} from "rxjs";

export interface ICellImage {
  image: Observable<any>;
  options?: Observable<{
    width?: string,
    height?: string,
    [key: string]: any
  }>
}

export interface ICellCustom {
  text: any;
  options?: {
    style?: {
      [key: string]: any
    },
  }
}

export interface ICellStatusChange {
  data: any;
  select: {
    options?: Array<{
      [key: string]: any
    }>;
    bindLabel: string;
    bindValue: string;
  }
}

export interface ICellCheckbox {
  data: any;
  label?: string;
}

export interface ICellPriority {
  data: any;
}
