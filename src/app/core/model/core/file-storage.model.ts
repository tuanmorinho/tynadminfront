import {IBaseModel} from "@core/model/core/base.model";

export interface IFileStorageBase extends IBaseModel {
  name?: string;
  type?: string;
  url?: string;
}

export interface IFileStorage extends IFileStorageBase {

}
