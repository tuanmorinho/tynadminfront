import {IBaseModel} from "@core/model/core";

export interface IAccountModel extends IBaseModel {
    fullName?: string;
    avatarUrl?: string;
    email?: string;
    mobile?: string;
}
