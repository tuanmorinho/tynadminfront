import {IBaseModel} from "@core/model/core";
import {ISurvey} from "@core/model/feature/survey.model";

export interface ISurveyCategoryBase extends IBaseModel {
  name?: string;
  description?: string;
  imageThumbnail?: string;
  imageFeature?: string;
  priority?: number;
  createdBy: number;
  createdDate: Date;
  modifiedBy: number;
  modifiedDate: Date;
}

export interface ISurveyCategory extends ISurveyCategoryBase {
  surveyCount?: number;
  surveyList?: ISurvey[];
}
