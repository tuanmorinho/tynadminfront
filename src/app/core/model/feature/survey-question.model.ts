import {IBaseModel} from "@core/model/core";

export interface ISurveyQuestionBase extends IBaseModel {
  surveyId?: number;
  name?: string;
  description?: string;
  imageThumbnail?: string;
  imageFeature?: string;
  answerDisplayType?: number;
  answerSelectType?: number;
  isAnswerRequired?: boolean;
  isFirst?: boolean;
  createdBy?: number;
  createdDate?: Date;
  modifiedBy?: number;
  modifiedDate?: Date;
}

export interface ISurveyQuestion extends ISurveyQuestionBase {
  answerCount?: number;
  answerList?: any[];
}

export interface ISurveyQuestionRequest extends ISurveyQuestionBase {

}

export const SURVEY_QUESTION_ANSWER_DISPLAY_TYPE = {
  ANSWER_DISPLAY_TYPE_LIST: { value: 1, label: 'Hiển thị dạng danh sách' },
  ANSWER_DISPLAY_TYPE_GRID: { value: 2, label: 'Hiển thị dạng lưới' }
}

export const SURVEY_QUESTION_ANSWER_DISPLAY_TYPE_LIST = [
  SURVEY_QUESTION_ANSWER_DISPLAY_TYPE.ANSWER_DISPLAY_TYPE_LIST,
  SURVEY_QUESTION_ANSWER_DISPLAY_TYPE.ANSWER_DISPLAY_TYPE_GRID
]

export const SURVEY_QUESTION_ANSWER_SELECT_TYPE = {
  ANSWER_SELECT_TYPE_ONE: { value: 1, label: 'Chọn một' },
  ANSWER_SELECT_TYPE_MULTI: { value: 2, label: 'Chọn nhiều' }
}

export const SURVEY_QUESTION_ANSWER_SELECT_TYPE_LIST = [
  SURVEY_QUESTION_ANSWER_SELECT_TYPE.ANSWER_SELECT_TYPE_ONE,
  SURVEY_QUESTION_ANSWER_SELECT_TYPE.ANSWER_SELECT_TYPE_MULTI
]
