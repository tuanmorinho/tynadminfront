import {IBaseModel} from "@core/model/core";

export interface ISurveyBase extends IBaseModel {
  surveyCategoryId?: number;
  code?: string;
  name?: string;
  slug?: string;
  description?: string;
  imageThumbnail?: string;
  imageFeature?: string;
  nextStepType?: number;
  status?: number;
  isTrash?: boolean;
  createdBy?: number;
  createdDate?: Date;
  modifiedBy?: number;
  modifiedDate?: Date;
}

export interface ISurvey extends ISurveyBase {
  questionCount?: number;
  questionList?: any[];
}

export interface ISurveyRequest extends ISurveyBase {
}

export const SURVEY_STATUS = {
  STATUS_ACTIVE: { value: 1, label: 'Đang hoạt động' },
  STATUS_INACTIVE: { value: 2, label: 'Ngừng hoạt động' }
}

export const SURVEY_STATUS_LIST = [
  SURVEY_STATUS.STATUS_ACTIVE,
  SURVEY_STATUS.STATUS_INACTIVE
]

export const SURVEY_NEXT_STEP_TYPE = {
  NEXT_STEP_TYPE_FLAT: { value: 1, label: 'Tất cả câu hỏi' },
  NEXT_STEP_TYPE_TREE: { value: 2, label: 'Step by step' }
}

export const SURVEY_NEXT_STEP_TYPE_LIST = [
  SURVEY_NEXT_STEP_TYPE.NEXT_STEP_TYPE_FLAT,
  SURVEY_NEXT_STEP_TYPE.NEXT_STEP_TYPE_TREE
]
