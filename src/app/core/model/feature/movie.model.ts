import {IBaseModel} from "@core/model/core";

export interface IMovie extends IBaseModel {
  title?: string;
  description?: string;
  imageThumbnail?: string;
  otherImages?: string;
  length?: number;
  releaseDate?: string;
  openingDate?: string;
  shownTillDate?: string;
  shownStartDate?: string;
  actors?: any;
  directors?: any[];
  languages?: any;
  nationals?: any[];
  rating?: any;
  tag?: string;
  tagColor?: string;
  tagWidth?: string;
}

export const MOVIE_TAG =  [
  { tag: 'HOT', des: 'Hot', color: '#D25E52' },
  { tag: 'OUTSTAND', des: 'Nổi bật', color: '#D9B43A' },
  { tag: 'OUTDATE', des: 'Dừng chiếu', color: 'gray' }
]
