import {ISurveyQuestion} from "@core/model/feature/survey-question.model";
import {ISurveyAnswer} from "@core/model/feature/survey-answer.model";

export interface ISurveyQuestionStep {
    surveyQuestionId: number;
    surveyQuestion?: ISurveyQuestion;
    surveyAnswer?: ISurveyAnswer[];
    step: Array<{
        surveyAnswerId: number;
        surveyAnswer?: ISurveyAnswer;
        surveyQuestionNextId: number;
        surveyQuestionNext?: ISurveyQuestion;
    }>;
}
