import {Observable, of, switchMap} from "rxjs";
import {ActivatedRouteSnapshot, Router} from "@angular/router";
import {inject} from "@angular/core";
import {SurveyQuestionService} from "@core/services/feature";
import {ROUTING_SURVEY_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";

export const surveyQuestionAddGuard = (route: ActivatedRouteSnapshot): Observable<boolean> => {
    const router: Router = inject<Router>(Router);
    const surveyQuestionService: SurveyQuestionService = inject<SurveyQuestionService>(SurveyQuestionService);

    return surveyQuestionService.validateCreateSurveyQuestion({surveyId: Number(route.queryParams['surveyId']) || undefined}).pipe(
        switchMap((permitted: boolean): Observable<boolean> => {
            if (!permitted) {
                void router.navigate([`${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_QUESTION}`], {
                    queryParams: {surveyId: route.queryParams['surveyId'] || null},
                });
                return of(false);
            }
            return of(true);
        })
    )
}
