import {inject} from '@angular/core';
import {
  Router
} from '@angular/router';
import {AuthService} from "@core/services/core/auth.service";
import {Observable, of, switchMap} from "rxjs";
import {ROUTING_TYN} from "@core/constants/routing-constant";

export const noAuthGuard = (): Observable<boolean> => {
  const authService: AuthService = inject<AuthService>(AuthService);
  const router: Router = inject<Router>(Router);

  return authService.checkAuth().pipe(
    switchMap((authenticated: boolean): Observable<boolean> => {
      if (authenticated) {
        router.navigate([`${ROUTING_TYN.DEFAULT}`]);
        return of(false);
      }
      return of(true);
    })
  )
}
