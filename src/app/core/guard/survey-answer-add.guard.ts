import {Observable, of, switchMap} from "rxjs";
import {ActivatedRouteSnapshot, Router} from "@angular/router";
import {inject} from "@angular/core";
import {SurveyAnswerService, SurveyQuestionService} from "@core/services/feature";
import {ROUTING_SURVEY_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";

export const surveyAnswerAddGuard = (route: ActivatedRouteSnapshot): Observable<boolean> => {
    const router: Router = inject<Router>(Router);
    const surveyAnswerService: SurveyAnswerService = inject<SurveyAnswerService>(SurveyAnswerService);

    return surveyAnswerService.validateCreateSurveyAnswer({
        surveyId: Number(route.queryParams['surveyId']) || undefined,
        surveyQuestionId: Number(route.queryParams['surveyQuestionId']) || undefined,
    }).pipe(
        switchMap((permitted: boolean): Observable<boolean> => {
            if (!permitted) {
                void router.navigate([`${ROUTING_TYN.SURVEY_PAGE}/${ROUTING_SURVEY_PAGE.SURVEY_ANSWER}`], {
                    queryParams: {
                        surveyId: route.queryParams['surveyId'] || null,
                        surveyQuestionId: Number(route.queryParams['surveyQuestionId']) || null,
                    },
                });
                return of(false);
            }
            return of(true);
        })
    )
}
