import {inject} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router, UrlSegment
} from '@angular/router';
import {AuthService} from "@core/services/core/auth.service";
import {Observable, of, switchMap} from "rxjs";
import {ROUTING_AUTH_PAGE, ROUTING_TYN} from "@core/constants/routing-constant";

export const authGuard = (route: ActivatedRouteSnapshot): Observable<boolean> => {
  const authService: AuthService = inject<AuthService>(AuthService);
  const router: Router = inject<Router>(Router);

  const redirectUrl: string = route.url.length === 0 || route.url[0].path === ROUTING_AUTH_PAGE.SIGN_OUT ? '/' : buildRedirectUrl(route)
    .map((v: UrlSegment) => v.path).reduce((a: string, b: string) => a + '/' + b, '');

  return authService.checkAuth().pipe(
    switchMap((authenticated: boolean): Observable<boolean> => {
      if (!authenticated) {
        router.navigate([`${ROUTING_TYN.AUTH_PAGE}/${ROUTING_AUTH_PAGE.SIGN_IN}`], {
          queryParams: { redirectUrl },
        });
        return of(false);
      }
      return of(true);
    })
  )
}

const buildRedirectUrl = (route: ActivatedRouteSnapshot, url: UrlSegment[] = []): UrlSegment[] => {
  const newRedirectUrlSegment: UrlSegment[] = [...url, ...route.url];
  if (route.firstChild) {
    return buildRedirectUrl(route.firstChild, newRedirectUrlSegment);
  }
  return newRedirectUrlSegment;
}
