import {
  Directive,
  inject,
  Input,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import {AuthService} from "@core/services/core";
import {IAuthUser} from "@core/model/core";

@Directive({
  selector: '[hasPermission]'
})
export class HasPermissionDirective {
  private permissions: string[] = [];
  private isHidden: boolean = true;

  private vcr: ViewContainerRef = inject<ViewContainerRef>(ViewContainerRef);
  private tpr: TemplateRef<any> = inject<TemplateRef<any>>(TemplateRef<any>);
  private authService: AuthService = inject<AuthService>(AuthService);

  @Input() set hasPermission(val: string[]) {
    this.permissions = val;
    this.checkView();
  }

  private checkView(): void {
    if (this.checkPermission()) {
      if (this.isHidden) {
        this.vcr.createEmbeddedView(this.tpr);
        this.isHidden = true;
      }
    } else {
      this.isHidden = false;
      this.vcr.clear();
    }
  }

  private checkPermission(): boolean {
    const currentUser: IAuthUser | any = this.authService.getUserByToken();

    if (currentUser && currentUser.roles.filter((role: string): boolean => role === 'ADMIN' || role === 'ROOT').length > 0) {
      return true;
    }

    let hasPermission: boolean = false;

    if (currentUser && currentUser.roles) {
      const currentPermissions = currentUser.roles || [];
      const permissionFound = currentPermissions.filter((role: string): boolean => this.permissions.indexOf(role) !== -1);
      if (permissionFound.length !== 0) {
        hasPermission = true
      } else hasPermission = false
    }
    return hasPermission;
  }
}
