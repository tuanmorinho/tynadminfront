import {Component, inject, OnDestroy} from '@angular/core';
import {Subject} from "rxjs";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {LoadingStoreService} from "@core/services/core";
import {FormGroup} from "@angular/forms";
import {environment} from "../../../../enviroments";

@Component({
  selector: 'app-base',
  template: `
    <div></div>`
})
export class BaseComponent implements OnDestroy {
  public _destroy$: Subject<void> = new Subject<void>();
  public form!: FormGroup;

  public router: Router = inject<Router>(Router);
  public loadingStore: LoadingStoreService = inject<LoadingStoreService>(LoadingStoreService);

  public baseUrlImage = environment.apiUrl.base;

  protected get queryParams() {
    return this.activatedRoute.queryParams;
  }

  protected get routeParams(): Params {
    return this.activatedRoute.snapshot.params;
  }

  private get activatedRoute(): ActivatedRoute {
    let route: ActivatedRoute = this.router.routerState.root;
    while (route.firstChild) {
      route = route.firstChild;
    }
    return route;
  }

  set isLoading(val: boolean) {
    this.loadingStore.isLoading = val;
  }

  get formControls() {
    return this.form.controls;
  }

  formControlStatus(name: string): boolean {
    return this.form.controls[name].invalid && this.form.controls[name].touched;
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }
}
