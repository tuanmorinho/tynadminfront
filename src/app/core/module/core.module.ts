import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from './base/base.component';
import {AuthModule} from "@core/module/auth/auth.module";
import {AppProviderFactory} from "@core/DI/init-factory";
import {AuthService} from "@core/services/core/auth.service";
import {DirectiveModule} from "@core/directive/directive.module";



@NgModule({
  declarations: [
    BaseComponent
  ],
  imports: [
    CommonModule,
    AuthModule
  ],
  exports: [
    AuthModule,
    DirectiveModule
  ],
  providers: [
    AuthService,
    AppProviderFactory
  ]
})
export class CoreModule { }
