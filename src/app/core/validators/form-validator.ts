import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export class CustomFormValidators {
  static emailDomainValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    const email = control.value as string;
    if (!email) {
      return null;
    }

    return email.match('[.][a-zA-Z]+$') ? null : {invalidEmailDomain: true};
  }

  static emailOrPhoneValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    return (!CustomFormValidators.emailValidator(control) || !CustomFormValidators.phoneValidator(control))
      ? null
      : {invalidEmailOrPhone: true};
  }

  static emailValidator(control: AbstractControl): ValidationErrors | null {
    const email = control.value as string;
    if (!email) {
      return null;
    }
    return email.match(
      // Email Standard RFC 5322:
      /^(([^<>~`!#$%^&*()=\?/{}'\[\]\\.,;:\s@"]+(\.[^<>~`!#$%^&*()=\?/{}'\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ // tslint:disable-line
    )
      ? null
      : {invalidEmail: true};
  }

  static phoneValidator(control: AbstractControl): ValidationErrors | null {
    const phone = control.value as string;
    if (!phone) {
      return null;
    }
    const isVNPhoneMobile = /^(0)(\d{9})$/;
    return isVNPhoneMobile.test(phone)
      ? null
      : {invalidPhone: true};
  }

  static passwordValidator(control: AbstractControl): ValidationErrors | null {
    const pwd = control.value as string;
    if (!pwd) {
      return null;
    }
    const mediumPwdRegex = new RegExp('^(?=.*[a-zA-Z])(?=.*[0-9])');
    return mediumPwdRegex.test(pwd)
      ? null
      : {invalidPassword: true};
  }

  // static specialCharacterValidator(control: AbstractControl): ValidationErrors | null {
  //     const value = control.value as string;
  //     if (!value) {
  //         return null;
  //     }
  //     const specialCharacterRegex = /[~`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  //     return specialCharacterRegex.test(value)
  //         ? {SpecialCharacter: true}
  //         : null;
  // }

  static specialCharacter(included = true, specialRegex = /[~`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value as string;
      if (!value) {
        return null;
      }
      const specialCharacterRegex = specialRegex;
      return specialCharacterRegex.test(value) !== included
        ? {[included ? 'includedSpecialCharacter' : 'excludedSpecialCharacter']: true}
        : null;
    }
  }

  static specialCharacterAndNumberValidator(control: AbstractControl): ValidationErrors | null {
    const value = control.value as string;
    if (!value) {
      return null;
    }
    const specialCharacterAndNumberRegex = /[0-9~`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    return specialCharacterAndNumberRegex.test(value)
      ? {SpecialCharacterAndNumber: true}
      : null;
  }

  static taxVatValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    const tax = control.value as string;
    if (!tax) {
      return null;
    }
    return tax.match('^[0-9-]*$') ? (tax.match(/-/g) || []).length <= 1 ? null : {invalidTax: true} : {invalidTax: true};
  }

  static taxVatValidatorMaxNumber(
    control: AbstractControl
  ): ValidationErrors | null {
    const tax = control.value as string;
    if (!tax) {
      return null;
    }
    return tax.includes('-') ? tax.length > 14 ? {invalidTaxVat: true} : null : tax.length <= 13 ? null : {invalidTaxVat: true};
  }

  static taxCompanyNameValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    const tax = control.value as string;
    if (!tax) {
      return null;
    }
    return tax.match('^[A-Za-z0-9-.&\',()ààááảảããạạâầầấấẩẩẫẫậậăằằắắẳẳẵẵặặèèééẻẻẽẽẹẹêềềếếểểễễệệđììííỉỉĩĩịịòòóóỏỏõõọọôồồốốổổỗỗộộơờờớớởởỡỡợợùùúúủủũũụụưừừứứửửữữựựỳỳýýỷỷỹỹỵỵÀÀÁÁẢẢÃÃẠẠÂẦẦẤẤẨẨẪẪẬẬĂẰẰẮẮẲẲẴẴẶẶÈÈÉÉẺẺẼẼẸẸÊỀỀẾẾỂỂỄỄỆỆĐÌÌÍÍỈỈĨĨỊỊÒÒÓÓỎỎÕÕỌỌÔỒỒỐỐỔỔỖỖỘỘƠỜỜỚỚỞỞỠỠỢỢÙÙÚÚỦỦŨŨỤỤƯỪỪỨỨỬỬỮỮỰỰỲỲÝÝỶỶỸỸỴỴ ]*$') ? null : {invalid: true};
  }

  static taxAddressValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    const tax = control.value as string;
    if (!tax) {
      return null;
    }
    return tax.match('^[A-Za-z0-9-.\',/ ààááảảããạạâầầấấẩẩẫẫậậăằằắắẳẳẵẵặặèèééẻẻẽẽẹẹêềềếếểểễễệệđììííỉỉĩĩịịòòóóỏỏõõọọôồồốốổổỗỗộộơờờớớởởỡỡợợùùúúủủũũụụưừừứứửửữữựựỳỳýýỷỷỹỹỵỵÀÀÁÁẢẢÃÃẠẠÂẦẦẤẤẨẨẪẪẬẬĂẰẰẮẮẲẲẴẴẶẶÈÈÉÉẺẺẼẼẸẸÊỀỀẾẾỂỂỄỄỆỆĐÌÌÍÍỈỈĨĨỊỊÒÒÓÓỎỎÕÕỌỌÔỒỒỐỐỔỔỖỖỘỘƠỜỜỚỚỞỞỠỠỢỢÙÙÚÚỦỦŨŨỤỤƯỪỪỨỨỬỬỮỮỰỰỲỲÝÝỶỶỸỸỴỴ]*$') ? null : {invalid: true};
  }

  static taxCustomerValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    const tax = control.value as string;
    if (!tax) {
      return null;
    }
    return tax.match('^[A-Za-z ààááảảããạạâầầấấẩẩẫẫậậăằằắắẳẳẵẵặặèèééẻẻẽẽẹẹêềềếếểểễễệệđììííỉỉĩĩịịòòóóỏỏõõọọôồồốốổổỗỗộộơờờớớởởỡỡợợùùúúủủũũụụưừừứứửửữữựựỳỳýýỷỷỹỹỵỵÀÀÁÁẢẢÃÃẠẠÂẦẦẤẤẨẨẪẪẬẬĂẰẰẮẮẲẲẴẴẶẶÈÈÉÉẺẺẼẼẸẸÊỀỀẾẾỂỂỄỄỆỆĐÌÌÍÍỈỈĨĨỊỊÒÒÓÓỎỎÕÕỌỌÔỒỒỐỐỔỔỖỖỘỘƠỜỜỚỚỞỞỠỠỢỢÙÙÚÚỦỦŨŨỤỤƯỪỪỨỨỬỬỮỮỰỰỲỲÝÝỶỶỸỸỴỴ]*$') ? null : {invalid: true};
  }
}
