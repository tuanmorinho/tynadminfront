export const API_BASE = 'http://localhost:8000/api/survey-recommender';

export const environment = {
  production: true,

  apiUrl: {
    base: API_BASE,
    // notification: API_BASE + '/api/notification',
  },
};
